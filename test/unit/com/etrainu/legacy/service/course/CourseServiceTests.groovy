package com.etrainu.legacy.service.course

import grails.test.GrailsUnitTestCase

import com.etrainu.legacy.model.training.Course
import com.etrainu.legacy.model.training.CourseDetails

class CourseServiceTests extends GrailsUnitTestCase {
	protected void setUp() {
		mockConfig '''
			maxGormRecords=50
		'''

		super.setUp()
	}

	protected void tearDown() {
		super.tearDown()
	}
	
	void testQuickSearch() {
		def courseDetailInstances = [
			new CourseDetails(
				searchData: "course one,111"
				, courseCode: "111"
				, inductionName: "course one"
				, archiveDate: Calendar.getInstance().getTime()
			),
			new CourseDetails(
				searchData: "course two,222"
				, courseCode: "222"
				, inductionName: "course two"
			),
			new CourseDetails(
				searchData: "course three,333"
				, courseCode: "333"
				, inductionName: "course three"
			)
		]
		mockDomain(CourseDetails, courseDetailInstances)
		
		// Initialise the service and test the target method.
		def testService = new CourseService()
		
		assertEquals 1, testService.quickSearch("one", true).maxResults
		//Test that archived courses don't show when we ask archived to not be available
		assertEquals 0, testService.quickSearch("one", false).maxResults
		assertEquals 1, testService.quickSearch("111", true).maxResults
		assertEquals 1, testService.quickSearch("two", true).maxResults
		//Test that the course definately isn't archived
		assertEquals 1, testService.quickSearch("two", false).maxResults
		assertEquals 1, testService.quickSearch("222", true).maxResults
		assertEquals 1, testService.quickSearch("three", true).maxResults
		assertEquals 1, testService.quickSearch("333", true).maxResults
		
		assertEquals 3, testService.quickSearch("course", true).maxResults
		
		CourseDetails c1 = testService.quickSearch("one", true).results[0]
		assertEquals "111", c1.courseCode
	}
	
	void testArchiveCourse() {
		def courseInstances = [
			new Course(
				id: "FCD1DB71-4974-4FEA-B0FE-00023820C2F4",
				name: "course one",
				desc: "testcourse",
				cost: 36.00
			)
		]

		mockDomain(Course, courseInstances)
		
		def testService = new CourseService()
		def testCourse = Course.get('FCD1DB71-4974-4FEA-B0FE-00023820C2F4')
		
		assertNull testCourse.archiveDate
		
		//archive the user
		testService.archiveCourse( testCourse )
		
		assertNotNull testCourse.archiveDate
		
		//make sure the archive date is right now (allow 3 seconds of variation for execution time..which is ridiculous but good enough)
		def theDate = new Date()
		def dateDiff = Math.abs ( ( theDate.time - testCourse.archiveDate.time ) / 1000 )
		assertTrue ( dateDiff <= 3 )
		
		//unarchive the user
		testService.unarchiveCourse(testCourse)
		
		assertNull testCourse.archiveDate
	}
}
