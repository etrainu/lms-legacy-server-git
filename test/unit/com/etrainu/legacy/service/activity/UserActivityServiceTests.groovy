package com.etrainu.legacy.service.activity

import java.util.Date;

import com.etrainu.legacy.activity.UserActivity;
import com.etrainu.legacy.activity.UserActivity.UserActivityType;
import com.etrainu.legacy.model.referrer.Transaction;
import com.etrainu.legacy.model.user.User;
import com.etrainu.legacy.model.user.UserAssessment;
import com.etrainu.legacy.model.user.UserCourse;
import com.etrainu.legacy.model.user.UserLogging;
import com.etrainu.legacy.model.user.UserScormAssessment;
import com.etrainu.legacy.service.activity.UserActivityService;

import grails.test.*

class UserActivityServiceTests extends GrailsUnitTestCase {
    
	def testService
	
	protected void setUp() {
        super.setUp()
		
		testService = new UserActivityService()
    }

    protected void tearDown() {
        super.tearDown()
    }

	void testAssessorActivity()
	{
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05')]
		
		final def assessorAssessments = (0..9).collect {new UserAssessment(id:it,fk_assessorUserID:1,isProficient:true,dtCompleted:testDates[it])}
		assessorAssessments.addAll((0..9).collect {new UserAssessment(id:it+32,fk_assessorUserID:2,isProficient:false,dtCompleted:testDates[it])})
		
		assertEquals 20, assessorAssessments.size()
				
		mockDomain(UserAssessment, assessorAssessments)
				
		//test the target method
		assertEquals 5, testService.assessorActivity(new User(id:1), null, null ).size()
		assertEquals 0, testService.assessorActivity(new User(id:2), null, null ).size()
		assertEquals 3, testService.assessorActivity(new User(id:1), new Date().parse('yyyy/MM/dd', '2011/01/01'), new Date().parse('yyyy/MM/dd', '2011/01/03') ).size()
		
		//Test one result in particular
		def start = new Date().parse('yyyy/MM/dd', '2011/01/01')
		def finish = new Date().parse('yyyy/MM/dd', '2011/01/01')
		assertEquals 1, testService.assessorActivity(new User(id:1), start, finish ).size()
		def testActivities = testService.assessorActivity(new User(id:1), start, finish)
		assertEquals 2 , testActivities[0].userActivityCount
		assertEquals UserActivityType.MARKING_COMPETENT , testActivities[0].userActivityType
		
	}
	
	
    void testAssessmentActivity() 
	{
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'), 
							 new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05')] 
		
		final def userAssessments = (0..9).collect {new UserAssessment(id:it,userId:1,inductionKey:it,dtStarted:testDates[it])}
		userAssessments.addAll((0..9).collect {new UserAssessment(id:it+32,userId:2,inductionKey:it,dtStarted:testDates[it])})
		
		assertEquals 20, userAssessments.size()
				
		mockDomain(UserAssessment, userAssessments)
				
		//test the target method
		assertEquals 5, testService.assessmentActivity(new User(id:1), null, null ).size()
		assertEquals 5, testService.assessmentActivity(new User(id:2), null, null ).size()
		
		assertEquals 3, testService.assessmentActivity(new User(id:2), new Date().parse('yyyy/MM/dd', '2011/01/01'), new Date().parse('yyyy/MM/dd', '2011/01/03') ).size()
		
		//Test one result in particular
		def start = new Date().parse('yyyy/MM/dd', '2011/01/01')
		def finish = new Date().parse('yyyy/MM/dd', '2011/01/01')
		assertEquals 1, testService.assessmentActivity(new User(id:2), start, finish ).size()
		def testActivities = testService.assessmentActivity(new User(id:2), start, finish)
		assertEquals 2 , testActivities[0].userActivityCount
		assertEquals UserActivityType.ASSESSMENT , testActivities[0].userActivityType
		
    }
	
	void testSCORMAssessmentActivity()
	{
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05')]
		
		final def userAssessments = (0..9).collect {new UserScormAssessment(id:it,userId:1,inductionKey:it,entryDate:testDates[it])}
		userAssessments.addAll((0..9).collect {new UserScormAssessment(id:it+32,userId:2,inductionKey:it,entryDate:testDates[it])})
		
		assertEquals 20, userAssessments.size()
				
		mockDomain(UserScormAssessment, userAssessments)
				
		//test the target method
		assertEquals 5, testService.scormAssessmentActivity(new User(id:1), null, null ).size()
		assertEquals 5, testService.scormAssessmentActivity(new User(id:2), null, null ).size()
		
		assertEquals 3, testService.scormAssessmentActivity(new User(id:2), new Date().parse('yyyy/MM/dd', '2011/01/01'), new Date().parse('yyyy/MM/dd', '2011/01/03') ).size()
		
		//Test one result in particular
		def start = new Date().parse('yyyy/MM/dd', '2011/01/01')
		def finish = new Date().parse('yyyy/MM/dd', '2011/01/01')
		assertEquals 1, testService.scormAssessmentActivity(new User(id:2), start, finish ).size()
		def testActivities = testService.scormAssessmentActivity(new User(id:2), start, finish)
		assertEquals 2 , testActivities[0].userActivityCount
		assertEquals UserActivityType.SCORM_ASSESSMENT , testActivities[0].userActivityType
		
	}
	
	void testLoginActivity()
	{
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05')]
		
		final def userlogins = (0..9).collect {new UserLogging(id:it, userId:1,logDate:testDates[it])}
		userlogins.addAll((0..9).collect {new UserLogging(id:it, userId:2,logDate:testDates[it])})
		
		assertEquals 20, userlogins.size()
				
		mockDomain(UserLogging, userlogins)
				
		//test the target method
		assertEquals 5, testService.loginActivity(new User(id:1), null, null ).size()
		assertEquals 5, testService.loginActivity(new User(id:2), null, null ).size()
		
		assertEquals 3, testService.loginActivity(new User(id:2), new Date().parse('yyyy/MM/dd', '2011/01/01'), new Date().parse('yyyy/MM/dd', '2011/01/03') ).size()
		
		//Test one result in particular
		def start = new Date().parse('yyyy/MM/dd', '2011/01/01')
		def finish = new Date().parse('yyyy/MM/dd', '2011/01/01')
		assertEquals 1, testService.loginActivity(new User(id:2), start, finish ).size()
		def testActivities = testService.loginActivity(new User(id:2), start, finish)
		assertEquals 2 , testActivities[0].userActivityCount
		assertEquals UserActivityType.LOGIN , testActivities[0].userActivityType
		
	}
	
	void testCourseAssigmnentActivity()
	{
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05')]
		
		final def userCourseAssignments = (0..9).collect {new UserCourse(id:it, userId:1,createDate:testDates[it])}
		userCourseAssignments.addAll((0..9).collect {new UserCourse(id:it, userId:2,createDate:testDates[it])})
			
		assertEquals 20, userCourseAssignments.size()
				
		mockDomain(UserCourse, userCourseAssignments)
				
		//test the target method
		assertEquals 5, testService.courseAssignmentActivity(new User(id:1), null, null ).size()
		assertEquals 5, testService.courseAssignmentActivity(new User(id:2), null, null ).size()
		
		assertEquals 3, testService.courseAssignmentActivity(new User(id:2), new Date().parse('yyyy/MM/dd', '2011/01/01'), new Date().parse('yyyy/MM/dd', '2011/01/03') ).size()
		
		//Test one result in particular
		def start = new Date().parse('yyyy/MM/dd', '2011/01/01')
		def finish = new Date().parse('yyyy/MM/dd', '2011/01/01')
		assertEquals 1, testService.courseAssignmentActivity(new User(id:2), start, finish ).size()
		def testActivities = testService.courseAssignmentActivity(new User(id:2), start, finish)
		assertEquals 2 , testActivities[0].userActivityCount
		assertEquals UserActivityType.COURSE_ASSIGNMENT , testActivities[0].userActivityType
		
	}
	
	void testPurchaseActivity()
	{
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05')]
		
		final def userPurchaseTransactions = (0..9).collect {new Transaction(id:it, purchasedByUserID:1,debitDate:testDates[it])}
		userPurchaseTransactions.addAll((0..9).collect {new Transaction(id:it, purchasedByUserID:2,debitDate:testDates[it])})
			
		assertEquals 20, userPurchaseTransactions.size()
				
		mockDomain(Transaction, userPurchaseTransactions)
				
		//test the target method
		assertEquals 5, testService.purchaseActivity(new User(id:1), null, null ).size()
		assertEquals 5, testService.purchaseActivity(new User(id:2), null, null ).size()
		
		assertEquals 3, testService.purchaseActivity(new User(id:2), new Date().parse('yyyy/MM/dd', '2011/01/01'), new Date().parse('yyyy/MM/dd', '2011/01/03') ).size()
		
		//Test one result in particular
		def start = new Date().parse('yyyy/MM/dd', '2011/01/01')
		def finish = new Date().parse('yyyy/MM/dd', '2011/01/01')
		assertEquals 1, testService.purchaseActivity(new User(id:2), start, finish ).size()
		def testActivities = testService.purchaseActivity(new User(id:2), start, finish)
		assertEquals 2 , testActivities[0].userActivityCount
		assertEquals UserActivityType.PURCHASE , testActivities[0].userActivityType
		
	}
}
