package com.etrainu.legacy.service.activity

import com.etrainu.legacy.model.referrer.Referrer
import com.etrainu.legacy.model.referrer.Transaction
import com.etrainu.legacy.activity.ReferrerActivity.ReferrerActivityType
import grails.test.*

class ReferrerActivityServiceTests extends GrailsUnitTestCase {
	
	ReferrerActivityService testService
	
    protected void setUp() {
        super.setUp()
		
		testService = new ReferrerActivityService()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testReferrerActivity() {
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'),
			new Date().parse('yyyy/MM/dd', '2011/01/01'),
			new Date().parse('yyyy/MM/dd', '2011/01/02'),
			new Date().parse('yyyy/MM/dd', '2011/01/02'),
			new Date().parse('yyyy/MM/dd', '2011/01/03'),
			new Date().parse('yyyy/MM/dd', '2011/01/03'),
			new Date().parse('yyyy/MM/dd', '2011/01/04'),
			new Date().parse('yyyy/MM/dd', '2011/01/04'),
			new Date().parse('yyyy/MM/dd', '2011/01/05'),
			new Date().parse('yyyy/MM/dd', '2011/01/05')]

			final def transactions = (0..9).collect {new Transaction(id:it,referrerDomain:"test",debitDate:testDates[it])}
			transactions.addAll((0..9).collect {new Transaction(id:it+32,referrerDomain:"test2",debitDate:testDates[it])})
			
			assertEquals 20, transactions.size()
			
			mockDomain(Transaction, transactions)
						
			//test the target method
			assertEquals 5, testService.salesActivity(new Referrer(id:"test",isDiscount:false), null, null ).size()
			assertEquals 5, testService.salesActivity(new Referrer(id:"test2", isDiscount:false), null, null ).size()
			
			assertEquals 3, testService.salesActivity(new Referrer(id:"test2"), new Date().parse('yyyy/MM/dd', '2011/01/01'), new Date().parse('yyyy/MM/dd', '2011/01/03') ).size()
			
			//Test one result in particular
			def start = new Date().parse('yyyy/MM/dd', '2011/01/01')
			def finish = new Date().parse('yyyy/MM/dd', '2011/01/01')
			assertEquals 1, testService.salesActivity(new Referrer(id:"test2"), start, finish ).size()
			def testActivities = testService.salesActivity(new Referrer(id:"test2"), start, finish)
			assertEquals 2 , testActivities[0].referrerActivityCount
			assertEquals ReferrerActivityType.SALES , testActivities[0].referrerActivityType

    }
	
	void testReferrerPromoCodeAndReferrerActivity() {
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'),
			new Date().parse('yyyy/MM/dd', '2011/01/01'),
			new Date().parse('yyyy/MM/dd', '2011/01/02'),
			new Date().parse('yyyy/MM/dd', '2011/01/02'),
			new Date().parse('yyyy/MM/dd', '2011/01/03'),
			new Date().parse('yyyy/MM/dd', '2011/01/03'),
			new Date().parse('yyyy/MM/dd', '2011/01/04'),
			new Date().parse('yyyy/MM/dd', '2011/01/04'),
			new Date().parse('yyyy/MM/dd', '2011/01/05'),
			new Date().parse('yyyy/MM/dd', '2011/01/05')]

			final def transactions = (0..9).collect {new Transaction(id:it,referrerDomain:"test",promoCode:"PROMO",debitDate:testDates[it])}
			transactions.addAll((0..9).collect {new Transaction(id:it+32,referrerDomain:"test2",debitDate:testDates[it])})
			
			assertEquals 20, transactions.size()
			
			mockDomain(Transaction, transactions)
						
			//test retrieval with various promo and referrer..
			assertEquals 5, testService.salesActivity(new Referrer(id:"PROMO",isDiscount:true), null, null ).size()
			assertEquals 0, testService.salesActivity(new Referrer(id:"PROMO", isDiscount:false), null, null ).size()
			assertEquals 5, testService.salesActivity(new Referrer(id:"test2", isDiscount:false), null, null ).size()
			assertEquals 0, testService.salesActivity(new Referrer(id:"test2", isDiscount:true), null, null ).size()
	}

}
