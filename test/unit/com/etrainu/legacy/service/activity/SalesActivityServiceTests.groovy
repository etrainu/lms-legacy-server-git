package com.etrainu.legacy.service.activity

import com.etrainu.legacy.model.referrer.Referrer
import com.etrainu.legacy.model.referrer.Transaction
import com.etrainu.legacy.activity.ReferrerActivity.ReferrerActivityType
import grails.test.*

class SalesActivityServiceTests extends GrailsUnitTestCase {
	
	SalesActivityService testService
	
    protected void setUp() {
        super.setUp()
		
		testService = new SalesActivityService()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSalesActivity() {
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'),
			new Date().parse('yyyy/MM/dd', '2011/01/01'),
			new Date().parse('yyyy/MM/dd', '2011/01/02'),
			new Date().parse('yyyy/MM/dd', '2011/01/02'),
			new Date().parse('yyyy/MM/dd', '2011/01/03'),
			new Date().parse('yyyy/MM/dd', '2011/01/03'),
			new Date().parse('yyyy/MM/dd', '2011/01/04'),
			new Date().parse('yyyy/MM/dd', '2011/01/04'),
			new Date().parse('yyyy/MM/dd', '2011/01/05'),
			new Date().parse('yyyy/MM/dd', '2011/01/05')]

			final def transactions = (0..9).collect {new Transaction(id:it,inductionId:"induction1",debitDate:testDates[it],amount:5)}
			transactions.addAll((0..9).collect {new Transaction(id:it+32,inductionId:"induction2",debitDate:testDates[it],amount:10)})
			
			assertEquals 20, transactions.size()
			
			mockDomain(Transaction, transactions)
						
			//test the target method : we expect two product
			Map MapKeyedByInductionId = testService.salesActivity(null, null )
			assertEquals 2, MapKeyedByInductionId.size()
			// 10 transactions for each induction but sales happen on 5 differents dates
			assertEquals 5, MapKeyedByInductionId["induction1"].size()
			assertEquals 5, MapKeyedByInductionId["induction2"].size()
			
			MapKeyedByInductionId["induction2"].each{ salesActivity -> 
				
				assertEquals "induction2" , salesActivity.inductionId
				assertEquals 2 , salesActivity.salesActivityCount
				assertEquals 20 , salesActivity.salesActivityIncome
			}
			
			MapKeyedByInductionId["induction1"].each{ salesActivity ->
				
				assertEquals "induction1" , salesActivity.inductionId
				assertEquals 2 , salesActivity.salesActivityCount
				assertEquals 10 , salesActivity.salesActivityIncome
			}
			
			//Test with Date range
			MapKeyedByInductionId = testService.salesActivity(new Date().parse('yyyy/MM/dd', '2011/01/01'), new Date().parse('yyyy/MM/dd', '2011/01/03') )
			assertEquals 2, MapKeyedByInductionId.size()
			assertEquals 3, MapKeyedByInductionId["induction1"].size()
			assertEquals 3, MapKeyedByInductionId["induction2"].size()
						
			MapKeyedByInductionId["induction2"].each{ salesActivity ->
				
				assertEquals "induction2" , salesActivity.inductionId
				assertEquals 2 , salesActivity.salesActivityCount
				assertEquals 20 , salesActivity.salesActivityIncome
			}
			
			MapKeyedByInductionId["induction1"].each{ salesActivity ->
				
				assertEquals "induction1" , salesActivity.inductionId
				assertEquals 2 , salesActivity.salesActivityCount
				assertEquals 10 , salesActivity.salesActivityIncome
			}
	}

}
