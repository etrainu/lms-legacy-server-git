package com.etrainu.legacy.service.activity

import grails.test.*

import com.etrainu.legacy.activity.CourseActivity.CourseActivityType
import com.etrainu.legacy.model.training.Course
import com.etrainu.legacy.model.user.UserCourse
import com.etrainu.legacy.service.activity.CourseActivityService

class CourseActivityServiceTests extends GrailsUnitTestCase 
{
    CourseActivityService testService
	
	protected void setUp() {
        super.setUp()
		
		testService = new CourseActivityService()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testCourseAssignmentActivity() 
	{
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'), 
							 new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05')] 
		
		final def userCourses = (0..9).collect {new UserCourse(id:it,courseId:1,accessAllowedDate:testDates[it])}
		userCourses.addAll((0..9).collect {new UserCourse(id:it+32,courseId:2,accessAllowedDate:testDates[it])})
		
		assertEquals 20, userCourses.size()
				
		mockDomain(UserCourse, userCourses)
				
		//test the target method
		assertEquals 5, testService.courseAssignmentActivity(new Course(id:1), null, null ).size()
		assertEquals 5, testService.courseAssignmentActivity(new Course(id:2), null, null ).size()
		
		assertEquals 3, testService.courseAssignmentActivity(new Course(id:2), new Date().parse('yyyy/MM/dd', '2011/01/01'), new Date().parse('yyyy/MM/dd', '2011/01/03') ).size()
		
		//Test one result in particular
		def start = new Date().parse('yyyy/MM/dd', '2011/01/01')
		def finish = new Date().parse('yyyy/MM/dd', '2011/01/01')
		assertEquals 1, testService.courseAssignmentActivity(new Course(id:2), start, finish ).size()
		def testActivities = testService.courseAssignmentActivity(new Course(id:2), start, finish)
		assertEquals 2 , testActivities[0].courseActivityCount
		assertEquals CourseActivityType.COURSE_ASSIGNMENT , testActivities[0].courseActivityType
		
    }
	
	void testCourseCompletionActivity()
	{
		def testDates = [ new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/01'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/02'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/03'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/04'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05'),
							 new Date().parse('yyyy/MM/dd', '2011/01/05')]
		
		final def userCourses = (0..9).collect {new UserCourse(id:it,courseId:1,inductionCompetentDate:testDates[it])}
		userCourses.addAll((0..9).collect {new UserCourse(id:it+32,courseId:2,inductionCompetentDate:testDates[it])})
		
		assertEquals 20, userCourses.size()
				
		mockDomain(UserCourse, userCourses)
				
		//test the target method
		assertEquals 5, testService.courseCompletionActivity(new Course(id:1), null, null ).size()
		assertEquals 5, testService.courseCompletionActivity(new Course(id:2), null, null ).size()
		
		assertEquals 3, testService.courseCompletionActivity(new Course(id:2), new Date().parse('yyyy/MM/dd', '2011/01/01'), new Date().parse('yyyy/MM/dd', '2011/01/03') ).size()
		
		//Test one result in particular
		def start = new Date().parse('yyyy/MM/dd', '2011/01/01')
		def finish = new Date().parse('yyyy/MM/dd', '2011/01/01')
		assertEquals 1, testService.courseCompletionActivity(new Course(id:2), start, finish ).size()
		def testActivities = testService.courseCompletionActivity(new Course(id:2), start, finish)
		assertEquals 2 , testActivities[0].courseActivityCount
		assertEquals CourseActivityType.COURSE_COMPLETION , testActivities[0].courseActivityType
		
	}
}
