package com.etrainu.legacy.service

import grails.test.GrailsUnitTestCase

import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.codehaus.groovy.grails.plugins.testing.GrailsMockHttpServletResponse

import com.etrainu.legacy.model.user.User
import com.etrainu.legacy.service.user.UserService
import com.etrainu.legacy.service.xml.RenderService

class ValidationServiceTests extends GrailsUnitTestCase 
{
	def testService
	def mockRenderService
	
	public void setUp() {
		super.setUp()
		
		testService = new ValidationService()
	}

	public void tearDown() {
		super.tearDown()
	}
	
	void testValidatePostRequest_failure() {
		mockRenderService = mockFor(RenderService)
		mockRenderService.demand.renderResult(1..1) { String arg1, int arg2 -> return false }
		
		testService.renderService = mockRenderService.createMock()
		
		def resp = testService.validatePostRequest( [xm:''] )
		assertFalse resp
	}
	
	void testValidatePostRequest_success() {
		def resp = testService.validatePostRequest( [xml:'<someXML>'] )
		assertTrue resp.success
	}
	
	void testValidateAPIUser_noHeader() {
		mockRenderService = mockFor(RenderService)
		mockRenderService.demand.renderResult(1..1) { String arg1, int arg2 -> return false }
		testService.renderService = mockRenderService.createMock()
		
		GrailsMockHttpServletResponse mockResponse = new GrailsMockHttpServletResponse()
		
		def resp = testService.validateAPIUser(mockResponse);
		
		assertFalse resp
	}
	
	void testValidateAPIUser_invalidHeader() {
		mockDomain User, [ new User(id:"FCD1DB71-4974-4FEA-B0FE-00023820C2F4", username:"alpha", authKey:"FCD1DB71-4974-4FEA-B0FE-00023820C2F4") ]
		
		mockRenderService = mockFor(RenderService)
		mockRenderService.demand.renderResult(1..1) { String arg1, int arg2 -> return false }
		testService.renderService = mockRenderService.createMock()
		
		GrailsMockHttpServletResponse mockResponse = new GrailsMockHttpServletResponse()
		mockResponse.setHeader('key', "foo")
		
		def resp = testService.validateAPIUser(mockResponse);
		
		assertFalse resp
	}
	
	void testValidateAPIUser_success() {	
		def mockedConfig = new ConfigObject()
		mockedConfig.authKeyTimeoutUnit = Calendar.HOUR
		mockedConfig.authKeyTimeoutValue = 1
		ConfigurationHolder.config = mockedConfig
		
		mockDomain User, [ new User(id:"FCD1DB71-4974-4FEA-B0FE-00023820C2F4", username:"alpha", authKey:"FCD1DB71-4974-4FEA-B0FE-00023820C2F4", authKeyLastUsed:Calendar.getInstance().getTime()) ]
		
		mockRenderService = mockFor(RenderService)
		mockRenderService.demand.renderResult(1..1) { String arg1, int arg2 -> return false }
		testService.renderService = mockRenderService.createMock()
		
		def mockUserService = mockFor(UserService)
		mockUserService.demand.saveUser(1..1) { User user -> return user }
		testService.userService = mockUserService.createMock()
		
		GrailsMockHttpServletResponse mockResponse = new GrailsMockHttpServletResponse()
		mockResponse.setHeader('key', "FCD1DB71-4974-4FEA-B0FE-00023820C2F4")
		
		def resp = testService.validateAPIUser(mockResponse);
		
		assertTrue resp.success
	}
}
