package com.etrainu.legacy.service.referrer

import com.etrainu.legacy.model.referrer.Referrer
import com.etrainu.legacy.model.referrer.ReferrerDetails
import grails.test.*

class ReferrerServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSearch() {
		def referrerInstance = [
			new ReferrerDetails(
				id: "google.com.au",
				name: "google",	
				isPercentage: 1,
				isDiscount: 0,
				discount: 20.00,
				totalPurchase: 3108,
				totalAmount: 246890.994,
				archiveDate: Calendar.getInstance().getTime()
			),
			new ReferrerDetails(
			id: "test.domain",
			name: "test domain",
			isPercentage: 1,
			isDiscount: 1,
			discount: 15.00,
			totalPurchase: 11000,
			totalAmount: 4567.994
		),
			new ReferrerDetails(
				id: "etrainu.com",
				name: "etrainu"	
			)
		]
		
		mockDomain(ReferrerDetails, referrerInstance)
		def testService = new ReferrerService()
		
		assertEquals 1, testService.searchReferrers("google", true).size
		//Test that archived courses are definately not included where required
		assertEquals 0, testService.searchReferrers("google", false).size
		assertEquals 1, testService.searchReferrers("etrainu", true).size
		//Test that non-archived courses show when includeArchived is false
		assertEquals 1, testService.searchReferrers("etrainu", false).size
		assertEquals 0, testService.searchReferrers("XXX", true).size
		assertEquals 3, testService.searchReferrers("", true).size
		
		assertEquals 1, testService.searchReferrers("test.domain", true).size
		assertEquals 1, testService.searchReferrers("test dom", true).size
		def results = testService.searchReferrers("t\\.", true)
		assertEquals 1, results.size
		final ReferrerDetails rDetails = results[0]
		assert rDetails.isPercentage
		assert rDetails.isDiscount
		assertEquals "test domain", rDetails.name
		assertEquals "test.domain", rDetails.id
		assertEquals 15.00, rDetails.discount
		assertEquals 11000, rDetails.totalPurchase
		assertEquals 4567.994, rDetails.totalAmount
		
    }
	
	void testGetReferrer() {
		final def archiveDate = new Date()
		
		def referrerInstance = [
			new Referrer(
				id: "test.com.au",
				name: "test",
				isPercentage: true,
				isDiscount: false,
				discount: 20.00,
				archiveDate: archiveDate
			)
		]
		
		def testService = new ReferrerService()
		def Referrer referrer
		
		mockDomain(Referrer, referrerInstance)

		referrer = testService.getReferrer("test")
		assertNull referrer
		
		referrer = testService.getReferrer("test.com.au")	
		assertNotNull referrer
		assertEquals "test", referrer.name	
		assertTrue referrer.isPercentage
		assertFalse referrer.isDiscount
		assertEquals 20.00, referrer.discount
		assertEquals archiveDate, referrer.archiveDate
	}
	
	void testSaveReferrer() {
		// Test Creating and updating of referrer
		
		def referrerInstance = [
			new Referrer(
				id: "test.com.au",
				name: "test",
				isPercentage: true,
				isDiscount: false,
				discount: 20.00,
				archiveDate: null
			)
		]
		
		def testService = new ReferrerService()
		def Referrer referrer
		
		mockDomain(Referrer, referrerInstance)
		
		// Domain size before
		assertEquals 1, Referrer.count()
		
		testService.saveReferrer(new Referrer(id: "test2.com.au", name: "test2", isPercentage: true, isDiscount: false, discount: 30.00))
		
		// Domain size after new referrer
		assertEquals 2, Referrer.count() 
		
		referrer = Referrer.get("test2.com.au")
		assertEquals "test2", referrer.name	
		assertTrue referrer.isPercentage
		assertEquals 30.00, referrer.discount
		
		// Test updating the referrer
		referrer.name = "test123"
		referrer.isPercentage = false
		referrer.discount = 20.00
		
		testService.saveReferrer(referrer)
		
		referrer = Referrer.get("test2.com.au")
		assertEquals "test123", referrer.name
		assertFalse referrer.isPercentage
		assertEquals 20.00, referrer.discount
		
		// Check that the first record is unchanged
		referrer = testService.getReferrer("test.com.au")
		assertNotNull referrer
		assertEquals "test", referrer.name
		assertTrue referrer.isPercentage
		assertFalse referrer.isDiscount
		assertEquals 20.00, referrer.discount
		assertNull referrer.archiveDate	
	}
	
	void testArchiveReferrer() {
		// Test archiving and unarchiving of referrer
		
		def referrerInstance = [
			new Referrer(
				id: "test.com.au",
				name: "test",
				isPercentage: true,
				isDiscount: false,
				discount: 20.00,
				archiveDate: null
			)
		]
		
		def testService = new ReferrerService()
		def Referrer referrer
		
		mockDomain(Referrer, referrerInstance)
		
		// Test archiving
		referrer = Referrer.get("test.com.au")	
		assertNull referrer.archiveDate
		testService.archiveReferrer(referrer)
		assertNotNull referrer.archiveDate

		// Test unarchiving
		testService.unarchiveReferrer(referrer)
		assertNull referrer.archiveDate
	}
	
}
