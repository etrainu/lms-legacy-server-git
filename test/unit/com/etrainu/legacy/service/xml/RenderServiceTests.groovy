package com.etrainu.legacy.service.xml

import com.etrainu.legacy.activity.SalesActivity;
import com.etrainu.legacy.model.training.Course;
import com.etrainu.legacy.service.course.CourseService;

import grails.test.*

class RenderServiceTests extends GrailsUnitTestCase {
    
	RenderService renderService
	
	protected void setUp() {
        super.setUp()
		
		renderService = new RenderService()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testSomething() {

		Map activityByProducts = [:]
		
		def testDates = ['2011/01/01' , '2011/01/01', '2011/01/02', 
						 '2011/01/02', '2011/01/03',  '2011/01/03',
						 '2011/01/04', '2011/01/04', '2011/01/05',
						 '2011/01/05']
		
		activityByProducts.put("induction1", (0..9).collect {new SalesActivity(salesActivityDate:testDates[it], inductionId:"induction1", salesActivityCount:it, salesActivityIncome:it*2)})
		activityByProducts.put("induction2", (0..9).collect {new SalesActivity(salesActivityDate:testDates[it], inductionId:"induction2", salesActivityCount:it*2, salesActivityIncome:it*4)})
		
		//We expect the CourseService.getCourse method to be called twice.	
		def mockCourseService = mockFor(CourseService)
		mockCourseService.demand.getCourse(1..2) { String arg1 -> return new Course(id:"${arg1}", name:"${arg1}Name") }

		//Inject Mock course Service
		renderService.courseService = mockCourseService.createMock()
		
		def response = renderService.renderSalesActivities(activityByProducts)
		def activities = new XmlParser().parseText(response.toString())
		
		assertEquals 2, activities.product.size() 
		assertEquals "induction1", activities.product[0].@id
		assertEquals "induction1Name", activities.product[0].@name
		
		assertEquals 10, activities.product[0].saleActivity.size()
		
		def i = 0
		activities.product[0].saleActivity.each{ 
			assertEquals testDates[i], it.@salesActivityDate	
			assertEquals "${i}", it.@salesActivityCount
			i++
		}
				
    }
}
