package com.etrainu.legacy.service.user

import grails.test.*

import java.util.Date

import com.etrainu.legacy.model.user.User
import com.etrainu.legacy.model.user.UserDetails
import com.etrainu.security.SecurityService

class UserServiceTests extends GrailsUnitTestCase {
	
	def mockSecurityService
	
    protected void setUp() {
		mockConfig '''
			maxGormRecords=50
		'''

        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }
	
	void testArchiveUser() {
		def userInstances = [
			new User(
				id: 'FCD1DB71-4974-4FEA-B0FE-00023820C2F4'
				, username: 'testUser'
				, firstname: 'test'
				, lastname: 'User'
				, acceptedTerms: true
				, password: 'test'
				, email: "test@test.com"
				, active: "1"
				, canArchive: false
				, groupId: 0
				, hideAds: 0
			)
		]

		mockDomain(User, userInstances)
		
		def testService = new UserService()
		def testUser = User.get('FCD1DB71-4974-4FEA-B0FE-00023820C2F4')
		
		assertNull testUser.archiveDate
		
		mockSecurityService = mockFor(SecurityService)
		mockSecurityService.demand.canAdministerUser(1..2) { User user, User targetUser -> return true }
		testService.securityService = mockSecurityService.createMock()
		
		//archive the user
		//Users can administer themselves so they should be able to archive themselves
		testService.archiveUser( testUser, testUser.id )
		
		assertNotNull testUser.archiveDate
		
		//make sure the archive date is right now (allow 3 seconds of variation for execution time..which is ridiculous but good enough)
		def theDate = new Date()
		def dateDiff = Math.abs ( ( theDate.time - testUser.archiveDate.time ) / 1000 )
		assertTrue ( dateDiff <= 3 )
		
		//unarchive the user
		testService.unarchiveUser(testUser, testUser.id )
		
		assertNull testUser.archiveDate
	}

    void testQuickSearch() {
		//User to execute the searches as
		def a = new User(
				id: 'FCD1DB71-4974-4FEA-B0FE-00023820C2F4'
				, username: 'testUser'
				, firstname: 'test'
				, lastname: 'User'
				, acceptedTerms: true
				, password: 'test'
				, email: "test@test.com"
				, active: "1"
				, canArchive: false
				, groupId: 0
				, hideAds: 0
		)
		def userInfoInstances = [ 
				//Admin
				new UserDetails(
					username: 'Userone'
					,firstname: 'oneFirst'
					,lastname: 'oneLast'
					,email: 'oneFirstoneLast@Gmail.com'
					,groupId:17
					,lastLogin: new Date()
					,participantGroupId: null
					,groupData: '17 , 0 , 0 , 0 '
					,searchData: 'oneFirst oneLast,Userone,oneFirstoneLast@Gmail.com',
					, archiveDate: Calendar.getInstance().getTime()
				)
				
				//B2B Participant
				, new UserDetails(
					username: 'UserTwo'
					,firstname: 'TwoFirst'
					,lastname: 'twoLast'
					,email: 'twoFirsttwoLast@Gmail.com'
					,groupId: 0
					,lastLogin: new Date()
					,participantGroupId: 175
					,groupData: '0 , 17 , 45 , 700 '
					,searchData: 'twoFirst twoLast,Usertwo,twoFirsttwoLast@Gmail.com'
				)
				
				//B2C Participant
				, new UserDetails(
					username: 'Userthree'
					,firstname: 'threeFirst'
					,lastname: 'threeLast'
					,email: 'threeFirstthreeLast@Gmail.com'
					,groupId: 0
					,lastLogin: new Date()
					,participantGroupId: null
					,groupData: '0 , 0 , 0 , 0 '
					,searchData: 'threeFirst threeLast,Userthree,threeFirstthreeLast@Gmail.com'
				)
				
				//B2C Participant
				, new UserDetails(
					username: 'Userfour'
					,firstname: 'fourFirst'
					,lastname: 'fourLast'
					,email: 'fourFirstfourLast@Gmail.com'
					,groupId: 0
					,lastLogin: new Date()
					,participantGroupId: null
					,groupData: '0 , 0 , 0 , 0 '
					,searchData: 'fourFirst fourLast,Userfour,fourFirstfourLast@Gmail.com'
				)
			]
		mockDomain(UserDetails, userInfoInstances)
		
		// Initialise the service and test the target method.
		def testService = new UserService()
		
		
		
		//Mock the criteria
		def detailsCriteria = [
			list: {Closure cls -> []}	
		]
		UserDetails.metaClass.static.createCriteria = {detailsCriteria}
		
		assertTrue true
		
		// Sadly, these tests are all completely useless because this entire function is driven by criteria...
		//Admin Tests
		/*assertEquals 1, testService.quickSearch(a, 'oneFirst', User.UserType.ADMIN, true, 0).maxResults
		//Ensure archived users don't appear when we don't want them to
		assertEquals 0, testService.quickSearch(a, 'oneFirst', User.UserType.ADMIN, false, 0).maxResults
		assertEquals 1, testService.quickSearch(a, 'oneFirst', User.UserType.ADMIN, true, 17).maxResults
		assertEquals 1, testService.quickSearch(a, '', User.UserType.ADMIN, true, 0).maxResults
		assertEquals 1, testService.quickSearch(a, '', User.UserType.ADMIN, true, 17).maxResults
		assertEquals 0, testService.quickSearch(a, 'fabio', User.UserType.ADMIN, true, 0).maxResults
		assertEquals 0, testService.quickSearch(a, 'fabio', User.UserType.ADMIN, true, 17).maxResults
		
		UserDetails u = testService.quickSearch(a, 'oneFirst', User.UserType.ADMIN, true, 0).results[0]
		assertEquals 'Userone',u.username
		
		//B2B Tests
		assertEquals 1, testService.quickSearch(a, 'twoFirst', User.UserType.B2B, true, 0).maxResults
		assertEquals 1, testService.quickSearch(a, 'twoFirst', User.UserType.B2B, true, 17).maxResults
		//Ensure that a non-archived user does appear when we don't want archived users
		assertEquals 1, testService.quickSearch(a, 'twoFirst', User.UserType.B2B, false, 17).maxResults
		assertEquals 1, testService.quickSearch(a, '', User.UserType.B2B, true, 0).maxResults
		assertEquals 1, testService.quickSearch(a, '', User.UserType.B2B, true, 17).maxResults
		assertEquals 0, testService.quickSearch(a, 'fabio', User.UserType.B2B, true, 0).maxResults
		assertEquals 0, testService.quickSearch(a, 'fabio', User.UserType.B2B, true, 17).maxResults
		
		UserDetails u2 = testService.quickSearch(a, 'twoFirst', User.UserType.B2B, true, 0).results[0]
		assertEquals 'UserTwo',u2.username
		
		//B2C Tests
		assertEquals 1, testService.quickSearch(a, 'threeFirst', User.UserType.B2C, true, 0).maxResults
		assertEquals 1, testService.quickSearch(a, 'threeFirst', User.UserType.B2C, true, 17).maxResults
		assertEquals 2, testService.quickSearch(a, '', User.UserType.B2C, true, 0).maxResults
		//Note that Group ID is ignored for B2C searches
		assertEquals 2, testService.quickSearch(a, '', User.UserType.B2C, true, 17).maxResults
		assertEquals 0, testService.quickSearch(a, 'fabio', User.UserType.B2C, true, 0).maxResults
		
		UserDetails u3 = testService.quickSearch(a, 'threeFirst', User.UserType.B2C, true, 0).results[0]
		assertEquals 'Userthree',u3.username
		*/
    }
}
