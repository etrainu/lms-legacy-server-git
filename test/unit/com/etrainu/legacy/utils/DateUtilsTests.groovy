package com.etrainu.legacy.utils

import org.codehaus.groovy.grails.commons.ConfigurationHolder;

import grails.test.*

class DateUtilsTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
		
		//mockConfig (''' etrainu.api.date.DATEFORMAT = "dd-mm-yyyy" ''')
		//mockConfig (''' etrainu.api.date.MAXRANGE = 365 ''')
		
		def mockedConfig = new ConfigObject()
		mockedConfig.etrainu.api.date.DATEFORMAT = "dd-mm-yyyy"
		mockedConfig.etrainu.api.date.MAXRANGE = 365
		ConfigurationHolder.config = mockedConfig
		
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testDateValidation() {

		def res = DateUtils.validateFromAndToDate(null,null)
		assert !res.hasErrors
		
		res = DateUtils.validateFromAndToDate("not a date",null)
		assert res.hasErrors
		
		//not dates
		res = DateUtils.validateFromAndToDate(null,"not a date")
		assert res.hasErrors
		
		//not dates
		res = DateUtils.validateFromAndToDate("not a date","not a date")
		assert res.hasErrors
		
		//Valid
		res = DateUtils.validateFromAndToDate("17-05-2011",null)
		assert !res.hasErrors
		
		//Valid
		res = DateUtils.validateFromAndToDate("17-05-2011","17-06-2011")
		assert !res.hasErrors
		
		//fromDate is after toDate
		res = DateUtils.validateFromAndToDate("17-06-2011","17-05-2010")
		assert res.hasErrors
		
		//date range exceed max configuration
		res = DateUtils.validateFromAndToDate("17-01-2009","18-01-2010")
		assert res.hasErrors
		
    }
	
	void testBetweenDays()
	{
		assertEquals 0, DateUtils.daysBetween(new Date(), new Date())
		assertEquals 1, DateUtils.daysBetween(new Date(), new Date()+1)
		assertEquals 365, DateUtils.daysBetween(Date.parse("dd-mm-yyyy", "17-05-1975"),Date.parse("dd-mm-yyyy", "17-05-1976"))
	}
}
