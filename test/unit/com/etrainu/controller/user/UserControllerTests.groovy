package com.etrainu.controller.user

import grails.test.*

import org.junit.*

import com.etrainu.legacy.model.user.User
import com.etrainu.legacy.model.user.UserCourse
import com.etrainu.legacy.model.user.UserDetails
import com.etrainu.legacy.model.user.UserPartnership

class UserControllerTests extends ControllerUnitTestCase 
{
	final def NONEXISTENTUSERID = "noneexistentuserid"
	final def u1 = new User(id:"FCD1DB71-4974-4FEA-B0FE-00023820C2F4", username:"alpha")
    final def u2 = new User(id:"FCD1DB71-4974-4FEA-B0FE-00023820C2F5", username:"beta")
	final def u3 = new User(username:"Gamma");
	
	final def ud1 = new UserDetails(id:"FCD1DB71-4974-4FEA-B0FE-00023820C2F4", username:"alpha")
    final def ud2 = new UserDetails(id:"FCD1DB71-4974-4FEA-B0FE-00023820C2F5", username:"beta")
	
	//Courses for p1
	final def uc1 = new UserCourse(id:"1",userId:"FCD1DB71-4974-4FEA-B0FE-00023820C2F4", providerGroupId:"P1", providerGroupName:"provider1");
	final def uc2 = new UserCourse(id:"2",userId:"FCD1DB71-4974-4FEA-B0FE-00023820C2F4", providerGroupId:"P2", providerGroupName:"provider2");
	
	//Courses for p2
	final def uc3 = new UserCourse(id:"3",courseId:"21",userId:"FCD1DB71-4974-4FEA-B0FE-00023820C2F5",providerGroupId:"P1");
	final def uc4 = new UserCourse(id:"4",courseId:"22",userId:"FCD1DB71-4974-4FEA-B0FE-00023820C2F5",providerGroupId:"P2");
	
	final def up1 = new UserPartnership(userId:"FCD1DB71-4974-4FEA-B0FE-00023820C2F4", groupId:4)
	final def up2 = new UserPartnership(userId:"FCD1DB71-4974-4FEA-B0FE-00023820C2F4", groupId:7)
	
	def mockRenderService
	def mockUserService
	def mockValidationService
	
	
	@Before
	public void setUp() {
        super.setUp()
		mockDomain User, [u1,u2]
		mockDomain UserCourse, [uc1,uc2,uc3,uc4]
		mockDomain UserPartnership, [up1,up2]
		mockDomain UserDetails, [ud1,ud2]
		
		mockRequest._apiUser = u1
    }

	@After
    public void tearDown() {
        super.tearDown()
    }
	
	// TODO: Heartbreakingly, due to massive structural changes, these tests all must be rewritten to be more suited to the thinned out version of the controller

	@Test
	void userForU1_post() {
		assertTrue true
	}

}
