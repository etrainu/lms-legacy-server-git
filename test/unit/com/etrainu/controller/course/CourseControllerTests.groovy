package com.etrainu.controller.course

import grails.test.*

import org.junit.*

import com.etrainu.legacy.model.training.Course
import com.etrainu.legacy.service.course.CourseService
import com.etrainu.legacy.service.xml.RenderService

class CourseControllerTests extends ControllerUnitTestCase {
	
	final def c1 = new Course(id: "FCD1DB71-4974-4FEA-B0FE-00023820C2F4", name: "course one", desc: "testcourse", cost: 36.00)
	
	def mockRenderService
	def mockCourseService

	@Before
	public void setUp() {
		super.setUp()
		mockDomain Course, [c1]
	}

	@After
	public void tearDown() {
		super.tearDown()
	}

	@Test
	void archiveForC1_get() {
		mockRenderService = mockFor(RenderService)
		mockRenderService.demand.renderCourse(1..1) {Course c -> return c.id}
		
		mockCourseService = mockFor(CourseService)
		mockCourseService.demand.archiveCourse(1..1) { Course c -> return c.id}
		
		controller.renderService = mockRenderService.createMock()
		controller.courseService = mockCourseService.createMock()
		controller.params.id = c1.id
		
		controller.archive()
		assert c1.id == controller.response.contentAsString
	}
}
