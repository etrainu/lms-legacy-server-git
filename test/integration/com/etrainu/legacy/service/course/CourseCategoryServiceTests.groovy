package com.etrainu.legacy.service.course

import grails.test.*

class CourseCategoryServiceTests extends GroovyTestCase {
    
	def categoryService 
	
	protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testretrievingCategories() 
	{
		def result = categoryService.getAllPublic()
		assertTrue result.size() > 0
    }
}
