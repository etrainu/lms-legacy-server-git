/*
 * This is a reloadable configuration file which can be edited on the fly and loaded by the application
 * Limitations:
 * Any referencing in this file must also be defined in this file. Take the following for example:
 * grails.legacy.base.url = "http://${grails.legacy.server.host}"
 * IF grails.legacy.server.host IS NOT DEFINED IN THIS FILE IT WILL RETURN [:] AS A BLANK CONFIG OBJECT WHEN THE CONFIG RELOADS
 */

//API Config
//etrainu.api.date.DATEFORMAT = "dd-MM-yyyy"
//etrainu.api.date.MAXRANGE = 365

//Maximum GORM/Hibernate Records
//maxGormRecords = 1000

//Timeout for authKeys on User
//authKeyTimeoutUnit = Calendar.HOUR
//authKeyTimeoutValue = 1