import org.springframework.jmx.export.MBeanExporter
import org.springframework.jmx.export.annotation.AnnotationJmxAttributeSource
import org.springframework.jmx.export.annotation.AnnotationMBeanExporter
import org.springframework.jmx.export.assembler.MetadataMBeanInfoAssembler
import org.springframework.jmx.export.naming.MetadataNamingStrategy
import org.springframework.jmx.support.MBeanRegistrationSupport

// Place your Spring DSL code here
beans = {

	annotationJmxAttributeSource(AnnotationJmxAttributeSource)

	jmxAnnotationsExporter(AnnotationMBeanExporter) {
		server = { org.springframework.jmx.support.MBeanServerFactoryBean sfb ->
			locateExistingServerIfPossible = true
		}
		autodetect = true
		autodetectMode = MBeanExporter.AUTODETECT_ASSEMBLER
		registrationBehavior = MBeanRegistrationSupport.REGISTRATION_IGNORE_EXISTING
		assembler = { MetadataMBeanInfoAssembler mbeanIA ->
			attributeSource = ref(annotationJmxAttributeSource)
		}
		namingStrategy = { MetadataNamingStrategy ns ->
			attributeSource = ref(annotationJmxAttributeSource)
		}
	}

	saltSource(org.springframework.security.authentication.dao.SystemWideSaltSource) { 
		systemWideSalt = 'B1#hb6)omIIQUltgQs#iBOA@NSQ8%v#BS,Z0!;!@h5G]' 
	}
}
