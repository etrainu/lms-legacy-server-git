//Using legacy MSSQL Server database
dataSource {
	pooled = true
	driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver"
	username = "db_restrictedUser"
	password = 'QUz-$2wr'
	logSql = true
}

//Using MYSQL 
//dataSource {
//	pooled = true
//	dbCreate = "update"
//	url = "jdbc:mysql://localhost/etrainu"
//	driverClassName = "com.mysql.jdbc.Driver"
//	dialect = org.hibernate.dialect.MySQL5InnoDBDialect
//	username = "root"
//	password = "root"
//}

hibernate {
	cache.use_second_level_cache = true
	cache.use_query_cache = true
	generate_statistics = true
	cache.provider_class = 'net.sf.ehcache.hibernate.EhCacheProvider'
	//flush.mode = "manual"
}
// environment specific settings
environments {
	development {
		dataSource {
			// USING Microsoft SQL Server
			//Commenting our dbCreate to instruct Grails to NOT update legacy DB
			//dbCreate = "create-drop" // one of 'create', 'create-drop','update'
			//etrainusql = dev server : the port below is dynamic and will change everytime the server instance get restarted
			dbCreate = 'none'
			username = "sa"
			password = "3Tra1nu"
			url = "jdbc:sqlserver://cn-cf-dev:1433;databaseName=etrainu"
						
			//USING LOCAL MYSQL DB
			//url = "jdbc:mysql://localhost/etrainu"
			//username = "root"
			//password = "root"
			//dbCreate = "create-drop" // one of 'create', 'create-drop','update'
			//logSql = true
		}
	}
	test {
		dataSource {
			// USING Microsoft SQL Server
			//Commenting our dbCreate to instruct Grails to NOT update legacy DB
			//dbCreate = "create-drop" // one of 'create', 'create-drop','update'
			//cn-cf8 = STAGING DB SERVER.
			url = "jdbc:sqlserver://cn-cf8:1433;databaseName=etrainu"
			
			//USING LOCAL MYSQL DB
			//url = "jdbc:mysql://localhost/etrainu"
//		    username = "root"
//			password = "root"
//			dbCreate = "create-drop" // one of 'create', 'create-drop','update'
//			logSql = true
			
		}
	}
	production {
		dataSource {
			// USING Microsoft SQL Server
			//Commenting our dbCreate to instruct Grails to NOT update legacy DB
			//dbCreate = "create-drop" // one of 'create', 'create-drop','update'
			dbCreate = 'none'
			username = 'db_restrictedUser'
			password = 'QUz-$2wr'
			url = "jdbc:sqlserver://ETR-DB:1433;databaseName=etrainu"
			logSql = true
		}
	}
	staging
	{
		dataSource {
			url = "jdbc:sqlserver://cn-cf8:1433;databaseName=etrainu"
			logSql = false
		}
	}
	steele {
		dataSource {
			url = "jdbc:sqlserver://CN-IT002:1433;databaseName=etrainu"
			logSql = false
		}
	}
	darren {
		dataSource {
			url = "jdbc:sqlserver://WEB-DEV2:1433;databaseName=etrainu"
			logSql = true
		}
	}
	tobias {
		dataSource {
			url = "jdbc:sqlserver://WEBDEV1-PC:1433;databaseName=etrainu"
			logSql = true
		}
	}
}
