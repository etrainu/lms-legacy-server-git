import com.etrainu.legacy.model.user.User

class UrlFilters {
	
	def validationService
	
	def filters = {
		controllerDetails(controller:'*', action:'*') {
			before = {

				//for some reason controllername is writing out as controller/action
				//Parse it in this case
				flash.controllerName = controllerName.toString().toLowerCase()
				if( flash.controllerName.matches(/^[a-z]+\/[a-z]+$/) ) {
					flash.controllerName = controllerName.find(/^[a-z]+/)
					flash.actionName = controllerName.find(/[a-z]+$/)
				} else {
					flash.actionName = actionName
				}
				
				def validationResponse

				//ControllerName may be null due to url mappings
				if( !flash.controllerName.equals('null') && !flash.controllerName.equals("security") ) {
					validationResponse = validationService.validateAPIUser( request )
			
					if( !validationResponse.success ) {
						render( validationResponse )
						return false
					}
				}

				if (request.method == "POST") {
					//check valid xml object
					validationResponse = validationService.validatePostRequest( params )
					if( !validationResponse.success ) {
						render( validationResponse )
						return false
					}
				}
				
				//cache the user hitting the API
				if (request.getHeader('key'))
				{
					request._apiUser = User.findByAuthKey( request.getHeader('key') )
				}
			}
		}
	}
}
