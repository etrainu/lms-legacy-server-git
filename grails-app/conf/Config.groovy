import org.apache.log4j.ConsoleAppender
import org.apache.log4j.DailyRollingFileAppender
import org.apache.log4j.Priority

// locations to search for config files that get merged into the main config
// config files can either be Java properties files or ConfigSlurper scripts

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if(System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.gorm.failOnError=true

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [ html: ['text/html','application/xhtml+xml'],
                      xml: ['text/xml', 'application/xml'],
                      text: 'text/plain',
                      js: 'text/javascript',
                      rss: 'application/rss+xml',
                      atom: 'application/atom+xml',
                      css: 'text/css',
                      csv: 'text/csv',
                      all: '*/*',
                      json: ['application/json','text/json'],
                      form: 'application/x-www-form-urlencoded',
                      multipartForm: 'multipart/form-data'
                    ]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// whether to install the java.util.logging bridge for sl4j. Disable for AppEngine!
grails.logging.jul.usebridge = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']


/**
* Directory configuration.
* Pickup the Tomcat/Catalina directory else use the target or current dir.
*/
def fs = File.separator // Local variable.
globalDirs.targetDir = new File("target${fs}").isDirectory() ? "target${fs}" : ''
globalDirs.catalinaBase = System.properties.getProperty('catalina.base')
globalDirs.logDirectory = globalDirs.catalinaBase ? "${globalDirs.catalinaBase}${fs}logs${fs}" : globalDirs.targetDir

//Default logging level for console and file appender
def log4jConsoleLogLevel = Priority.WARN
def log4jAppFileLogLevel = Priority.INFO

// set per-environment serverURL stem for creating absolute links
environments {
	development
	{
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
		grails.serverURL = "http://localhost:8090/${appName}"
	}
	test
	{
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
		grails.serverURL = "http://localhost:8090/${appName}"
	}
	production
	{
		grails.serverURL = "http://www.changeme.com"
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
	}
	staging
	{
		//Overrides for Staging
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
		grails.serverURL = "http://localhost:8090/${appName}"
	}
	steele
	{
		log4jConsoleLogLevel = Priority.DEBUG
		log4jAppFileLogLevel = Priority.DEBUG
		grails.serverURL = "http://localhost:$serverPort}/${appName}"
	}
}

//////////////////////////////////////////////////////////////////
// log4j configuration
//////////////////////////////////////////////////////////////////

log4j = {
  println "Log4j consoleLevel: ${log4jConsoleLogLevel} appFile Level: ${log4jAppFileLogLevel}"
 
  //def logLayoutPattern = new PatternLayout("%d [%t] %-5p %c %x - %m%n")
 
  error 'org.codehaus.groovy.grails.commons', // core / classloading
		  'org.codehaus.groovy.grails.plugins', // plugins
		  'org.codehaus.groovy.grails.orm.hibernate', // hibernate itg
		  'org.springframework',
		  'org.hibernate',
		  'net.sf.ehcache.hibernate',
		  'grails',
		  'groovyx.net.http'
 
  info 'org.codehaus.groovy.grails.web.servlet',  //  controllers
		  'org.codehaus.groovy.grails.web.pages', //  GSP
		  'org.codehaus.groovy.grails.web.sitemesh', //  layouts
		  'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
		  'org.codehaus.groovy.grails.web.mapping' // URL mapping
 
  debug 'grails.app.controller', //actual controllers
  		'com.etrainu',
  	    'org.apache.http.headers',
        'org.apache.http.wire'
		
		/* This is the list of grails.app artifacts for logging
		 * grails.app.bootstrap
		 * grails.app.dataSource
		 * grails.app.tagLib
		 * grails.app.service
		 * grails.app.controller
		 * grails.app.domain
		 */
		
 
  appenders {
	appender new ConsoleAppender(name: "${appName}-console",
		    threshold: log4jConsoleLogLevel,
			layout:pattern(conversionPattern: '[%t] %-5p %c{2} %x - %m%n')
	)
	appender new DailyRollingFileAppender(name: "${appName}",
			file:"${globalDirs.logDirectory}${appName}.log".toString(),
			threshold: log4jAppFileLogLevel,
			datePattern: "'.'yyyy-MM-dd",
			layout:pattern(conversionPattern: '%d{[EEE, dd-MMM-yyyy @ HH:mm:ss.SSS]} [%t] %-5p %c %x - %m%n')
	)
  }
 
  root {
	error "${appName}-console", "${appName}"
	additivity = true
  }
}

// TODO: Comment when we figure out how to deploy the reloadable appConfig to a war file
//API Config
etrainu.api.date.DATEFORMAT = "dd-MM-yyyy"
etrainu.api.date.MAXRANGE = 365

//Maximum GORM/Hibernate Records
maxGormRecords = 1000

//Timeout for authKeys on User
authKeyTimeoutUnit = Calendar.HOUR
authKeyTimeoutValue = 1

grails.config.locations = ["file:./AppConfig.groovy"]
grails.plugins.reloadConfig.files = []
grails.plugins.reloadConfig.includeConfigLocations = true
grails.plugins.reloadConfig.interval = 5000
grails.plugins.reloadConfig.enabled = true
grails.plugins.reloadConfig.notifyPlugins = []