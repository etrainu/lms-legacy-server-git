package com.etrainu.controller.course

import groovy.lang.Closure
import groovy.util.ConfigObject

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import com.etrainu.legacy.model.training.Course
import com.etrainu.legacy.model.training.CourseDetails
import com.etrainu.search.SearchResult


class CourseController {
	
	static final private String NO_ID_SPECIFIED = "You must include an id(?id=) parameter to access this resource."
	static final private String NOT_FOUND = "Could not find course"
	
	final private ConfigObject config = ConfigurationHolder.config
	def renderService
    def courseService
	
	def index = {
		redirect(action: "search", params: params)
	}
	
	def search =
	{
		final courseList
		switch(request.method) {
			case "GET":
				courseList = performQuickSearch(params)
				return render(text:renderService.renderSearchResult(courseList), contentType:"text/xml", encoding:"UTF-8")
				
				break
			case "POST":
				courseList = performSearch(params)
				final def XMLResponse = renderService.renderCourses(courseList)
				return render(text:XMLResponse.toString(), contentType:"text/xml", encoding:"UTF-8")
				
				break
		}
	}
	
	def get =
	{
		withCourse { course ->
			return render(text:renderService.renderCourse(course), contentType:"text/xml", encoding:"UTF-8")
		}
	}

	def private performSearch(params)
	{
		List courseList
		if( !params.id ) {
			def includeArchived = false;
			try { 
				includeArchived = params.includeArchived.toBoolean()
			} catch(Throwable e) {}
			
			courseList = courseService.searchCourses(params.criteria, includeArchived)
		} else {
			courseList = [Course.findById(params.id)]
		}

		return courseList
	}
	
	def private performQuickSearch(params)
	{
		SearchResult courseList 
		def maxResults = ( params.maxResults ) ? Integer.parseInt( params.maxResults ) : config.maxGormRecords;
		
		if( !params.id ) {
			def includeArchived = true;
			try { 
				includeArchived = params.includeArchived.toBoolean()
			} catch(Throwable e) {}
			
			courseList = courseService.quickSearch(params.searchString, includeArchived, maxResults)
		} else {
		    courseList = new SearchResult()
			courseList.parseQuery([CourseDetails.findById(params.id)], 1)
		}
		
		return courseList
	}
	
	def archive= {
		withCourse{ course ->
			courseService.archiveCourse course
			render(text:renderService.renderCourse(course), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
	def unarchive= {
		withCourse{ course ->
			courseService.unarchiveCourse course
			render(text:renderService.renderCourse(course), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
	private def withCourse(id="id", Closure c) {
		
		if( !params[id]) {
			return render(renderService.renderResult( NO_ID_SPECIFIED , renderService.BadRequest))
		}
		
		final def Course course = Course.get(params[id])
		if(course) {
			c.call course
		} else {
			render(renderService.renderResult( NOT_FOUND , renderService.BadRequest))
		}
	}
}
