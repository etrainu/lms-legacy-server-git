package com.etrainu.controller.course

import groovy.lang.Closure

import com.etrainu.legacy.model.training.Category
import com.etrainu.legacy.model.training.Course
import com.etrainu.legacy.model.training.CourseInCategory

class CategoryController {
	
	static final private String NO_ID_SPECIFIED = "You must include an id(?id=) parameter to access this resource."
	static final private String NOT_FOUND = "Could not find category"
	
    def categoryService
    def renderService
	
	def list =
	{
		def categories = categoryService.getAllPublic()
		render(text:renderService.renderCategories(categories), contentType:"text/xml", encoding:"UTF-8")
	}
	
	def courseList =
	{
		withCategory { category ->
			def courseIds = CourseInCategory.findAllByCategoryIdOrParentCategoryId( category.id, category.id )
			final def courses = courseIds.collect { Course.get( it.inductionId) }
			
//			courseIds.each {
//				courses.add Course.get( it.inductionId)
//			}
			
			render(text:renderService.renderCourses(courses), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
	private def withCategory(id="id", Closure c) {
		
		if( !params[id]) {
			return render(renderService.renderResult( NO_ID_SPECIFIED , renderService.BadRequest))
		}
		
		final def Category category = Category.get(params[id])
		if(category) {
			c.call category
		} else {
			render(renderService.renderResult( NOT_FOUND + " [${params[id]}]", renderService.BadRequest))
		}
	}
}
