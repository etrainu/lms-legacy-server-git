package com.etrainu.controller.course

import com.etrainu.legacy.model.training.Category
import com.etrainu.legacy.model.training.Course

class CourseToBuyController {

    def courseCategoryService
	
    def index = {
		redirect(action: "list", params: params)
	}
	
    def list = 
	{
		def categories = courseCategoryService.loadAllPublicCategories()	
		def Category cat = categories.find { it.id == params.id }
		def courses = cat.courses
		[courses : courses]
	}
	
	def show = 
	{
		def course = Course.findById(params.id)
		[course:course]	
	}
}
