package com.etrainu.controller.security

import com.etrainu.legacy.model.user.User

class SecurityController {

	def renderService
	def validationService
	def springSecurityService
	def saltSource
	def userService

	//This is for the mobile app
	def login = {
		if(!params.user || !params.pass) {
			return render(renderService.renderResult("GET Login request must include username and password. Example: /etrainu-server/security/login?u=username&p=password", renderService.BadRequest))
		}

		def username = params.user
		def password = params.pass

		def user = User.findByUsernameAndPassword(username, password)

		if( !user ) {
			return render(renderService.renderResult("Invalid Username or Password", renderService.BadRequest))
		}

		return render(text:renderService.renderUser(user), contentType:"text/xml", encoding:"UTF-8", status: renderService.Ok)
	}
	
	def authenticate = {
		if(!params.user || !params.pass) {
			return render(renderService.renderResult("GET Login request must include username and password. Example: /etrainu-server/security/login?u=username&p=password", renderService.BadRequest))
		}
		
		def username = params.user
		def password = params.pass

		def user = User.findByUsername(username)
		
		//encrypt the pw for comparison
		if( !user || !password.equals( springSecurityService.encodePassword( user.getPassword(), saltSource.getSystemWideSalt()) ) ) {
			return render(renderService.renderResult("Invalid Username or Password", renderService.BadRequest))
		}
		
		return render(renderService.renderResult(user.generateAuthKey(), renderService.Ok))
	}
	
	def userForAuthentication = {
		//Lock this method to etrainu only!
		def validationResponse = validationService.validateEtrainuAPIUser( request )

		if( !validationResponse.success ) {
			return render( validationResponse )
		}
		
		if( !params.user ) {
			return render(renderService.renderResult("userForAuthentication must include username. Example: /etainu-server/security/userForAuthentication?user=", renderService.BadRequest))
		}
		
		def user = User.findByUsername(params.user)
		
		if( !user ) {
			return render(renderService.renderResult("Invalid Username or Password", renderService.BadRequest))
		}
		
		// Hard coded the rendering as we only want username and password for this, that is all.
		StringWriter XMLResponse = new StringWriter()
		def builder = renderService.getXMLBuilder(XMLResponse)
		
		//Hard coded as we want very limited info here
		builder.user {
			id(user.id)
			username(user.username)
			password( springSecurityService.encodePassword( user.getPassword(), saltSource.getSystemWideSalt()) )
			active(user.active)
			archiveDate( user.archiveDate )
			permissions( user.permissions )
		}
		
		return render(text: XMLResponse.toString()
			, contentType:"text/xml"
			, encoding:"UTF-8"
			, status:200)
	}
}

