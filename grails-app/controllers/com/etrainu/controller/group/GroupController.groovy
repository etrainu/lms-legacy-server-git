package com.etrainu.controller.group

import com.etrainu.legacy.model.group.Group

class GroupController {

	def renderService
	
	def list = 
	{
		/*
		* If @id is not available
		* 	Return all available Organisation groups
		* Else
		*  Return all available child groups for specified group
		*/
		
		if (!params.id) {
			// List all Organisations
			def g = Group.createCriteria()
			def groupOrgs = g.list {
				and {
					isNull('archiveDate')
					eq('fk_groupTypeId', 3)
				}
			}
			//def groupOrgs = Group.findAllByFk_groupTypeIdAndArchiveDate([3,'NULL'])
			render(text:renderService.renderGroups(groupOrgs), contentType:"text/xml", encoding:"UTF-8")
			
		} else {
			// List all child groups for specified group
		
			def group = Group.get(params.id)
			
			if(group) {
				def groupChildren = Group.findAllByParentGroup(group.id)
				render(text:renderService.renderGroups(groupChildren), contentType:"text/xml", encoding:"UTF-8")
			} else {
				render(text:renderService.renderMessage("Could not find group [${params.id}]"), contentType:"text/xml", encoding:"UTF-8", status:"202")
				return
			}
		
		}
		
	}
	
	def search =
	{
		if(!params.g) {
			render(renderService.renderResult("Request must include a groupName param. Example: /etrainu-server/group/search/?g=groupName", renderService.BadRequest))
			return false
		} else {
			def searchString = '%' + params.g + '%'
			def groups = Group.findAllByGroupNameLike(searchString)
			render(text:renderService.renderGroups(groups), contentType:"text/xml", encoding:"UTF-8")
		}	
	}
}
