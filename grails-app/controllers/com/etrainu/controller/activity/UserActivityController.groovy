package com.etrainu.controller.activity

import groovy.lang.Closure

import com.etrainu.legacy.model.user.User
import com.etrainu.legacy.utils.DateUtils
import com.etrainu.legacy.utils.UserUtils
import com.etrainu.legacy.validation.DateValidationResponse

class UserActivityController {

	def renderService
	def userActivityService
	def validationService
	
	def all=
	{
		withUser{user ->
			def res = checkDates(params)
			if( res.hasErrors ) { return }
		
			def activities = userActivityService.assessmentActivity(user, res.fromDate, res.toDate)
			activities.addAll(userActivityService.scormAssessmentActivity(user, res.fromDate, res.toDate))
			activities.addAll(userActivityService.loginActivity(user, res.fromDate, res.toDate))
			activities.addAll(userActivityService.courseAssignmentActivity(user, res.fromDate, res.toDate))
			activities.addAll(userActivityService.assessorActivity(user, res.fromDate, res.toDate))
			activities.addAll(userActivityService.purchaseActivity(user, res.fromDate, res.toDate))
			render(text:renderService.renderUserActivities(activities), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
	def purchaseActivity =
	{
		withUser{user ->
			def res = checkDates(params)
			def activities = userActivityService.purchaseActivity(user, res.fromDate, res.toDate)
			render(text:renderService.renderUserActivities(activities), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
    def assementActivity=
	{
		withUser{user ->
			def res = checkDates(params)
			def activities = userActivityService.assessmentActivity(user, res.fromDate, res.toDate)
			activities.addAll(userActivityService.scormAssessmentActivity(user, res.fromDate, res.toDate))
			render(text:renderService.renderUserActivities(activities), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
	def loginActivity =
	{
		withUser{user ->
			def res = checkDates(params)
			def activities = userActivityService.loginActivity(user, res.fromDate, res.toDate)
			render(text:renderService.renderUserActivities(activities), contentType:"text/xml", encoding:"UTF-8")
		}
	}

	def courseActivity =
	{
		withUser{user ->
			def res = checkDates(params)
			def activities = userActivityService.courseAssignmentActivity(user, res.fromDate, res.toDate)
			render(text:renderService.renderUserActivities(activities), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
	def assessorActivity = 
	{
		withUser{user ->
			def res = checkDates(params)
			def activities = userActivityService.assessorActivity(user, res.fromDate, res.toDate)
			render(text:renderService.renderUserActivities(activities), contentType:"text/xml", encoding:"UTF-8")
		}
	}

	private DateValidationResponse checkDates(Map params) {
		final DateValidationResponse res = DateUtils.validateFromAndToDate(params.fromDate,params.toDate)
		if (res.hasErrors)
		{
			render(renderService.renderResult(res.errorMessage, renderService.Unsupported))
		}
		return res
	}
	
	private def withUser(id="id", Closure c) {
		
		if( !params[id]) {
			return render(renderService.renderResult("You must include an id(?id=) parameter to access this resource.", renderService.BadRequest))
		}
		
		final User user = UserUtils.findUserbyUUIDorUsername(params[id])
		if(user) {
			c.call user
		} else {
			render(renderService.renderResult("Could not find user [${params[id]}]", renderService.BadRequest))
		}
	}
}
