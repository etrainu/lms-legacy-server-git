package com.etrainu.controller.activity

import java.util.Map

import com.etrainu.legacy.utils.DateUtils
import com.etrainu.legacy.validation.DateValidationResponse

class SalesActivityController {
	
	def renderService
	def salesActivityService
	def validationService

    def sales = 
	{
		final DateValidationResponse res = checkDates(params)
		final Map activityMapKeyedByInduction = salesActivityService.salesActivity(res.fromDate, res.toDate)
		render(text:renderService.renderSalesActivities(activityMapKeyedByInduction), contentType:"text/xml", encoding:"UTF-8")
	}
	
	private DateValidationResponse checkDates(Map params) {
		final DateValidationResponse res = DateUtils.validateFromAndToDate(params.fromDate,params.toDate)
		if (res.hasErrors)
		{
			return render(renderService.renderResult(res.errorMessage, renderService.Unsupported))
		}
		return res
	}
}
