package com.etrainu.controller.activity

import groovy.lang.Closure
import java.util.Map
import com.etrainu.legacy.model.referrer.Referrer
import com.etrainu.legacy.utils.DateUtils
import com.etrainu.legacy.validation.DateValidationResponse

class ReferrerActivityController {
	
	def renderService
	def referrerActivityService
	def validationService

    def sale = 
	{
		withReferrer{referrer ->
			DateValidationResponse res = checkDates(params)
			def activities = referrerActivityService.salesActivity(referrer, res.fromDate, res.toDate)
			render(text:renderService.renderReferrerActivities(activities), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
	private def withReferrer(id="id", Closure c) {
		if( !params[id]) {
			return render(renderService.renderResult("You must include an id(?id=) parameter to access this resource.", renderService.BadRequest))
		}
		
		final Referrer referrer = Referrer.get(params[id])
		if(referrer) {
			c.call referrer
		} else {
			render(renderService.renderResult("Could not find referrer [${params[id]}]", renderService.BadRequest))
		}
	} 
	
	private DateValidationResponse checkDates(Map params) {
		final DateValidationResponse res = DateUtils.validateFromAndToDate(params.fromDate,params.toDate)
		if (res.hasErrors)
		{
			return render(renderService.renderResult(res.errorMessage, renderService.Unsupported))
		}
		return res
	}
}
