package com.etrainu.controller.activity

import groovy.lang.Closure
import com.etrainu.legacy.model.training.Course
import com.etrainu.legacy.utils.DateUtils
import com.etrainu.legacy.validation.DateValidationResponse

class CourseActivityController {

    def renderService
	def courseActivityService
	
	def all=
	{
		withCourse{course ->
			DateValidationResponse res = checkDates(params)
			def activities = courseActivityService.courseAssignmentActivity(course, res.fromDate, res.toDate)
			activities.addAll(courseActivityService.courseCompletionActivity(course, res.fromDate, res.toDate))
			render(text:renderService.renderCourseActivities(activities), contentType:"text/xml", encoding:"UTF-8")
		}
	}

	def completion = 
	{
		withCourse{course ->
			DateValidationResponse res = checkDates(params)
			def activities = courseActivityService.courseCompletionActivity(course, res.fromDate, res.toDate)
			render(text:renderService.renderCourseActivities(activities), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
	def assignment =
	{
		withCourse{course ->
			DateValidationResponse res = checkDates(params)
			def activities = courseActivityService.courseAssignmentActivity(course, res.fromDate, res.toDate)
			render(text:renderService.renderCourseActivities(activities), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
	private def withCourse(id="id", Closure c) {
		
		if( !params[id]) {
			return render(renderService.renderResult("You must include an id(?id=) parameter to access this resource.", renderService.BadRequest))
		}
		
		final Course course = Course.findById(params[id])
		if(course) {
			c.call course
		} else {
			render(renderService.renderResult("Could not find course [${params[id]}]", renderService.BadRequest))
		}
	}
	
	private DateValidationResponse checkDates(Map params) {
		final DateValidationResponse res = DateUtils.validateFromAndToDate(params.fromDate,params.toDate)
		if (res.hasErrors)
		{
			return render(renderService.renderResult(res.errorMessage, renderService.Unsupported))
		}
		return res
	}
	
}
