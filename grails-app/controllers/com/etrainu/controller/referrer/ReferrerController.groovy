package com.etrainu.controller.referrer

import grails.converters.XML
import groovy.lang.Closure

import com.etrainu.legacy.model.referrer.Referrer

class ReferrerController {
	
	static final private String NO_ID_SPECIFIED = "You must include an id(?id=) parameter to access this resource."
	static final private String NO_NAME_SPECIFIED = "You must include an name(?name=) parameter to access this resource."
	static final private String NOT_FOUND = "Could not find referrer"
	
	def renderService
	def referrerService
	
    def index = {
		redirect(action: "search", params: params)
	}

	def referrer = {
		switch(request.method)
		{
			case "GET":	
				// Get referrer details from referrer id
				List referrerList = []
				
				withReferrer{ referrer ->
					referrerList.add(referrer)
					render(text:renderService.renderReferrers(referrerList), contentType:"text/xml", encoding:"UTF-8")
				}
			break
			case "POST":
				def referrer
				def xml = XML.parse(params.xml)
				
				if(xml.@id.text()) {
					referrer = Referrer.get(xml.@id.text())
	
					if(!referrer) {
						return render(renderService.renderResult("Invalid referrerID '" + xml.@id.text() + "'", renderService.BadRequest))
					}
	
					referrer.update xml
					if(!referrerService.saveReferrer(referrer)) {
						println "Failed to add/update referrer"					
						return render(renderService.renderResult("An error has occured and the referrer could not be updated", renderService.BadRequest))
					}
				}
				else
				{
					referrer = new Referrer();
					referrer.id = xml.domain.text();
					referrer.update xml
	
					if(Referrer.get(referrer.id)) {
						return render(renderService.renderResult("Referrer with referrerID '" + referrer.id + "' already exists", renderService.BadRequest))
					}
					
					if(Referrer.findByName(referrer.name)) {
						return render(renderService.renderResult("Referrer with display name '" + referrer.name + "' already exists", renderService.BadRequest))
					}
	  
					if(!referrerService.saveReferrer(referrer)) {
						println "Failed to add/update referrer"
						
						return render(renderService.renderResult("An error has occured and the referrer could not be added", renderService.BadRequest))
					}
				}
				
				render(text:renderService.renderReferrer(referrer), contentType:"text/xml", encoding:"UTF-8")
			break
			default:
				return render(renderService.renderResult("Unknown Request", renderService.BadRequest))
			break
		}	
	}
	
	def archive = {
		List referrerList = []
		
		withReferrer{ referrer ->
			referrerList.add(referrer)
						
			if(!referrerService.archiveReferrer(referrer)) {
				println "Failed to add/update referrer"
				
				return render(renderService.renderResult("An error has occured and the referrer could not be archived", renderService.BadRequest))
			}
			
			render(text:renderService.renderReferrer(referrerList), contentType:"text/xml", encoding:"UTF-8")
		}	
	}
	
	def unarchive = {
		List referrerList = []
		
		withReferrer{ referrer ->
			referrerList.add(referrer)
			
			if(!referrerService.unarchiveReferrer(referrer)) {
				println "Failed to unarchive referrer"
				
				return render(renderService.renderResult("An error has occured and the referrer could not be unarchived", renderService.BadRequest))
			}
			render(text:renderService.renderReferrer(referrerList), contentType:"text/xml", encoding:"UTF-8")
		}
	}
	
	def search =
	{
		final referrerList
		if(params){
				referrerList = performSearch(params)
				final def XMLResponse = renderService.renderReferrers(referrerList)
				return render(text:XMLResponse.toString(), contentType:"text/xml", encoding:"UTF-8")
		}
		else
		{
			return referrerList
		}
	}
	
	def private performSearch(params)
	{
		List referrerList
		
		def includeArchived = false;
		try { 
			includeArchived = params.includeArchived.toBoolean()
		} catch(Throwable e) {}
		
		//if null returns full result set
		referrerList = referrerService.searchReferrers(params.name, includeArchived)
		
		if(params.id){
			referrerList = referrerService.searchReferrers(params.id, true)
		}

		return referrerList
	}
	
	private def withReferrer(id="id", Closure c) {
		
		if(!params[id]) {
			return render(renderService.renderResult( NO_ID_SPECIFIED , renderService.BadRequest))
		}
		
		final def Referrer referrer = Referrer.get(params[id])
		if(referrer) {
			c.call referrer
		} else {
			return render(renderService.renderResult( NOT_FOUND , renderService.BadRequest))
		}
	}
}
