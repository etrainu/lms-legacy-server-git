package com.etrainu.controller.user

import grails.converters.XML

import com.etrainu.legacy.model.user.User
import com.etrainu.legacy.model.user.UserDetails
import com.etrainu.legacy.utils.UserUtils
import com.etrainu.search.SearchResult

class UserController {
	
	static final private String NO_ID_SPECIFIED = "You must include an id(?id=) parameter to access this resource."
	static final private String NOT_FOUND = "Could not find user"
	
	def renderService
	def userService
	def springSecurityService
	def securityService
	def saltSource
	def validationService

	def provider = {
		try {
			def providerMap = userService.getProviders( request._apiUser, params.id )
			render(text:renderService.renderProviders(providerMap), contentType:"text/xml", encoding:"UTF-8")
		} catch (Throwable e) {
			render( renderService.renderResult(e.message, renderService.BadRequest ))
		}	
	}
	
	def archive= {
		try {
			def user = userService.archiveUser( request._apiUser, params.id )
			render(text:renderService.renderXMLSerializable(user), contentType:"text/xml", encoding:"UTF-8")
		} catch (Throwable e) {
			render( renderService.renderResult(e.message, renderService.BadRequest ))
		}
	}
	
	def unarchive= {
		try {
			def user = userService.unarchiveUser( request._apiUser, params.id )
			render(text:renderService.renderXMLSerializable(user), contentType:"text/xml", encoding:"UTF-8")
		} catch (Throwable e) {
			render( renderService.renderResult(e.message, renderService.BadRequest ))
		}
	}

	def course= {
		try {
			def userCourses = userService.getCourses( request._apiUser, params.id, params.providerId, params.key )
			render(text:renderService.renderUserCourses(userCourses), contentType:"text/xml", encoding:"UTF-8")
		} catch (Throwable e) {
			render( renderService.renderResult(e.message, renderService.BadRequest ))
		}
	}

	// TODO: Turn etrainu only validtion into annotation
	def editPermissions = {
		//Lock this method to etrainu only!
		def validationResponse = validationService.validateEtrainuAPIUser( request )

		if( !validationResponse.success ) {
			return render( validationResponse )
		}
		
		def xml = XML.parse( params.xml )
		
		try {
			def user = userService.updatePermissions( request._apiUser, xml )
			render(text:renderService.renderXMLSerializable(user), contentType:"text/xml", encoding:"UTF-8")
		} catch (Throwable e) {
			render( renderService.renderResult(e.message, renderService.BadRequest ))
		}
	}

	def user = {
		try {
			switch(request.method)
			{
				case "GET":
					def user = userService.getUser( request._apiUser, params.id )
					render(text:renderService.renderXMLSerializable(user), contentType:"text/xml", encoding:"UTF-8")
				break	
				case "POST":
					// TODO: Data Segregation
					// TODO: Move to service
					def gUser
					def xml = XML.parse( params.xml )
	
					if( xml.id.text() ) {
						gUser = User.get(xml.id.text())
	
						if( !gUser ) {
							return render(renderService.renderResult("Invalid UserID", renderService.BadRequest))
						}
	
						gUser.update xml
						userService.saveUser gUser					
					} 
					else 
					{
						gUser = new User();
						gUser.username = xml.username.text();
						gUser.update xml
	
						if( User.findByUsername( gUser.username) ) {
							return render(renderService.renderResult("User with username " + gUser.username + " already exists", renderService.BadRequest))
						}
	
						userService.saveUser gUser
					}
					render(text:renderService.renderUser(gUser), contentType:"text/xml", encoding:"UTF-8")
					
				break
				default:
					return render(renderService.renderResult("Unknown Request", renderService.BadRequest))
				break
			}
		} catch (Throwable e) {
			render( renderService.renderResult(e.message, renderService.BadRequest ))
		}
	}
	
	def partnership = {
		try {
			def userPartnerships = userService.getPartnerships( request._apiUser, params.id )
			render(text:renderService.renderUserPartnerships(userPartnerships), contentType:"text/xml", encoding:"UTF-8")
		} catch (Throwable e) {
			render( renderService.renderResult(e.message, renderService.BadRequest ))
		}
	}
	
	// TODO: Data Segregation
	def search=
	{
		try {
			def searchResult = performQuickSearch(request._apiUser, params)
			return render(text:renderService.renderSearchResult(searchResult), contentType:"text/xml", encoding:"UTF-8")
		} catch (Throwable e) {
			throw e
			render( renderService.renderResult(e.message, renderService.BadRequest ))
		}
	}
	

	def private performQuickSearch(User user, params)
	{
		SearchResult userList
		if( !params.id ) {
			//this stuff now has data segregation, it does still need a bit of a cleanup though
			def searchParam = ( params.searchString ) ? params.searchString.toString() : "";
			def userParam = User.UserType.ADMIN
			def groupParam = ( params.gid ) ? Integer.parseInt(params.gid) : 0;
			def includeArchived = false;
			try { 
				includeArchived = params.archived.toBoolean()
			} catch(Throwable e) {}
			def maxResults = ( params.maxResults ) ? Integer.parseInt( params.maxResults ) : 20;
	
			if( params.type ) {
				try {
					userParam = User.UserType.valueOf( params.type.toUpperCase() )
				} catch(e) { 
					log.warn "Unrecognized user type found (" + params.type.toUpperCase() + ")"
				}
			}
			
			userList = userService.quickSearch(user, searchParam, userParam, groupParam, includeArchived, maxResults)
		} else {
			// TODO: User service.getUserDetails
			// TODO: Data Segregation
			withUserDetails{ userDetails ->
				userList = new SearchResult();
				userList.parseQuery([userDetails], 1)
			}
		}
		
		return userList
	}

	// TODO: Remove when controller no longer requires
	private def withUser(id="id", Closure c) {
		
		if( !params[id]) {
			return render(renderService.renderResult( NO_ID_SPECIFIED , renderService.BadRequest))
		}
		
		final def User user = UserUtils.findUserbyUUIDorUsername(params[id])
		if(user) {
			c.call user
		} else {
			render(renderService.renderResult( NOT_FOUND + " [${params[id]}]", renderService.BadRequest))
		}
	}
	
	//Use where needed to avoid having to hit the db twice to validate userdetails
	// TODO: Copy to Service
	// TODO: Data Segregation
	// TODO: Remove when controller no longer requires
	private def withUserDetails(id="id", Closure c) {
		if( !params[id] ) {
			return render(renderService.renderResult( NO_ID_SPECIFIED , renderService.BadRequest))
		}
		
		final def UserDetails userDetails = UserDetails.get(params[id])
		if(userDetails) {
			c.call userDetails
		} else {
			render(renderService.renderResult( NOT_FOUND + " [${params[id]}]", renderService.BadRequest))
		}
	}
}
