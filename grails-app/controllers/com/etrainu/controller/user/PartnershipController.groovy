package com.etrainu.controller.user

import grails.converters.XML

import com.etrainu.exception.security.SecurityViolationException

class PartnershipController {
	
	def renderService
	def partnershipService
	
	def partnership = {
		try {
			switch( request.method ) {
				case "GET":
					render( partnershipService.withPartnership(request._apiUser, params.id, { partnership ->
							return [text:renderService.renderXMLSerializable(partnership), contentType:"text/xml", encoding:"UTF-8"]
						})
					)
				break
				case "POST":
					def xml = XML.parse( params.xml )
					def partnership
					
					if( xml.id.text() ) {
						partnership = partnershipService.updatePartnership(request._apiUser,xml)
					} else {
						partnership = partnershipService.createPartnership(request._apiUser,xml)
					}
					
					render( [text:renderService.renderXMLSerializable(partnership), contentType:"text/xml", encoding:"UTF-8"] )
				break
				default:
					render(renderService.renderResult("Unknown Request", renderService.BadRequest))
				break
			}
		} catch (Throwable e) {
			render( renderService.renderResult(e.message, renderService.BadRequest ))
		}
	}
	
	def archive= {
		try {
			def partnership = partnershipService.archivePartnership(request._apiUser,params.id)
			return render( [text:renderService.renderXMLSerializable(partnership), contentType:"text/xml", encoding:"UTF-8"] )
		} catch (Throwable e) {
			render( renderService.renderResult(e.message, renderService.BadRequest ))
		}
	}
}
