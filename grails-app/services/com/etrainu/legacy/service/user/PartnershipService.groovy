package com.etrainu.legacy.service.user

import groovy.lang.Closure
import groovy.util.slurpersupport.GPathResult

import org.codehaus.groovy.grails.exceptions.InvalidPropertyException
import org.springframework.transaction.annotation.Transactional

import com.etrainu.exception.security.SecurityViolationException
import com.etrainu.legacy.model.user.Partnership
import com.etrainu.legacy.model.user.User

class PartnershipService {

	static transactional = true
	
	def renderService
	def securityService
	
	static final private String NO_ID_SPECIFIED = "You must include an id (?id=) parameter to access this resource."
	static final private String NOT_FOUND = "Could not find partnership"
	static final private String NO_PERMISSION = "You do not have permission to access this partnership"
	static final private String NO_PERMISSION_CREATE = "You do not have permission to administer this user or group"
	
	@Transactional(readOnly = false)
	def savePartnership(Partnership partnership) {
		if(partnership.save(failOnError:true)) {
			return partnership
		}
		else {
			partnership.errors.allErrors.each { println it }
		}
	}
	
	@Transactional(readOnly = false)
	def archivePartnership(User user, String id) {
		withPartnership(user, id,{ partnership ->
			
			partnership.archiveDate = new Date()

			return savePartnership( partnership )
		})
	}
	
	/* partnerships cannot be unarchived, a new one must be created */
	@Transactional(readOnly = false)
	def updatePartnership(User user, GPathResult xml) {
		withPartnership(user, xml.id.text(), { partnership ->
			partnership.update xml
			
			if( !securityService.canAdministerPartnership(user, partnership) ) {
				log.warn 'User['+user.username+'] prevented from modifying partnership ['+partnership.toString()+'].'
				partnership.discard()
				throw new SecurityViolationException( NO_PERMISSION_CREATE )
			}
			
			return savePartnership(partnership)
		})
	}
	
	@Transactional(readOnly = false)
	def createPartnership(User user, GPathResult xml) {
		def partnership = new Partnership()
		
		partnership.update xml
		
		if( !securityService.canAdministerPartnership(user, partnership) ) {
			log.warn 'User['+user.username+'] prevented from creating partnership ['+partnership.toString()+'].'
			partnership.discard()
			throw new SecurityViolationException( NO_PERMISSION_CREATE )
		}
			
		return savePartnership(partnership)
	}
	
	@Transactional(readOnly = false)
	private def withPartnership(User user, String id, Closure c) {
		if( !id ) throw new MissingPropertyException( NO_ID_SPECIFIED )
		
		final def partnership = Partnership.get(id)
		
		if(!partnership) throw new InvalidPropertyException( NOT_FOUND + " [${id}]" )
		
		if( !securityService.canAdministerPartnership(user, partnership) ) {
			log.warn 'User['+user.username+'] prevented from accessing partnership ['+partnership.toString()+'].'
			throw new SecurityViolationException( NO_PERMISSION )
		}
		
		c.call partnership
	}
}
