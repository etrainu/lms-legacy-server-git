package com.etrainu.legacy.service.user

import groovy.lang.Closure
import groovy.util.slurpersupport.GPathResult

import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH
import org.codehaus.groovy.grails.exceptions.InvalidPropertyException
import org.springframework.transaction.annotation.Transactional

import com.etrainu.exception.security.SecurityViolationException
import com.etrainu.legacy.model.user.User
import com.etrainu.legacy.model.user.UserCourse
import com.etrainu.legacy.model.user.UserDetails
import com.etrainu.legacy.model.user.UserPartnership
import com.etrainu.legacy.utils.MonitoringUtils
import com.etrainu.search.SearchResult

class UserService {

	static transactional = true
	
	def securityService
	
	static final private String NO_ID_SPECIFIED = "You must include an id (?id=) parameter to access this resource."
	static final private String NOT_FOUND = "Could not find user"
	static final private String NO_PERMISSION = "You do not have permission to access this user"
	
	@Transactional(readOnly = true)
	def getUser(User user, String id) {
		withUser(user, id) { tUser ->
			return tUser
		}
	}

	@Transactional(readOnly = false)
	def saveUser(User user) {
		if(user.save(failOnError:true)) {
			return user
		}
		else {
			user.errors.allErrors.each { println it }
		}
	}
	
	@Transactional(readOnly = false)
	def archiveUser(User user, String id) {
		withUser(user, id) { tUser ->
			tUser.archiveDate = new Date()

			return saveUser( tUser )
		}
	}
	
	@Transactional(readOnly = false)
	def unarchiveUser(User user, String id) {
		withUser(user, id) { tUser ->
			tUser.archiveDate = null

			return saveUser( tUser )
		}
	}
	
	@Transactional(readOnly = true)
	def getProviders(User user, String id) {
		withUser(user, id) { tUser ->
			def userCourses = UserCourse.findAllByUserId(tUser.id)
			def providerMap = [:]
			userCourses.each { uc -> providerMap.put(uc.providerGroupId, uc.providerGroupName)}
			return providerMap
		}
	}
	
	@Transactional(readOnly = false)
	def updatePermissions(User user, GPathResult xml) {
		withUser(user, xml.id.text()) { tUser ->
			tUser.permissions = xml.permissions.text()
			return saveUser(tUser)
		}
	}
	
	@Transactional(readOnly = true)
	def getCourses(User user, String id, String providerId, String key) {
		withUser(user, id) { tUser ->
			 if ( providerId ) {
				 //Find all courses for this user and provider
				 return UserCourse.findAllByUserIdAndProviderGroupId(tUser.id,providerId)
			 }
			 else if ( key ) {
				 //Find a particular course for a specific user
				 return UserCourse.findAllByIdAndUserId(key,tUser.id)
			 }
			 else {
				 //Find all courses for this user
				 return UserCourse.findAllByUserId(tUser.id)
			 }
		 }
	}
	
	@Transactional(readOnly = true)
	def getPartnerships(User user, String id) {
		withUser(user, id) { tUser ->
			if( tUser.id != user.id ) {
				return UserPartnership.createCriteria().list{
					eq('userId', tUser.id)
					or {
						eq('groupId', tUser.groupId) /*special allowance to manage partnerships with your own group */
						sqlRestriction "groupid in ("+securityService.groupSegregationSql(user.groupId)+")"
					}
				}
			} else {
				return UserPartnership.findAllByUserId(tUser.id)
			}
		}
	}
	
	
	@Transactional(readOnly = true)
	def quickSearch(User user, String searchString, User.UserType userType, Boolean includeArchived, Integer groupID) {
		return quickSearch(user, searchString, userType, groupID, includeArchived, CH.config.maxGormRecords)
	}

	@Transactional(readOnly = true)
	def quickSearch(User user, String searchString, User.UserType userType, Integer groupID, Boolean includeArchived, Integer maxResults) {
		MonitoringUtils.benchmark("quickSearch") {
			def result = new SearchResult()
			def users
			def criteria = UserDetails.createCriteria()
			
			// TODO: I'm sure with a bit more thought this function could be simplified a bit more
			if( userType == User.UserType.ADMIN ) {
				users = criteria.list {
//					
					if( !includeArchived ) {
						isNull('archiveDate')
					}
					if( searchString.length() ) {
						ilike("searchData", '%'+searchString+'%')
					}
					
					if( groupID == 0) 
						ge("groupId",0)
					else
						sqlRestriction "fk_groupId in ("+securityService.groupSegregationSql(groupID)+")"
						
					//Note MaxResults is CASE-SENSITIVE. do not use .list(max: because that causes crazy paging functions which cripple the db
					MaxResults(CH.config.maxGormRecords)
					order("firstname")
				}
			}
			
			if( userType == User.UserType.B2B ) {
				users = criteria.list {
					if( !includeArchived ) {
						isNull('archiveDate')
					}
					if( groupID == 0) {
						isNotNull("participantGroupId")
					} else {
						ilike("groupData", '% ' + groupID.toString() + ' %')
					}
					if( searchString.length() ) {
						ilike("searchData", '%'+searchString+'%')
					}
					if( !securityService.isAdministrator(user) ) {
						sqlRestriction "participantGroupId in ("+securityService.groupSegregationSql(user.groupId)+")"
					}
					MaxResults(CH.config.maxGormRecords)
					order("firstname")
				}
			}
			
			if( userType == User.UserType.B2C ) {
				users = criteria.list {
					eq("groupData", '0 , 0 , 0 , 0 ')
					if( !includeArchived ) {
						isNull('archiveDate')
					}
					if( searchString.length() ) {
						ilike("searchData", '%'+searchString+'%')
					}
					if( !securityService.isAdministrator(user) ) {
						sqlRestriction "participantGroupId in ("+securityService.groupSegregationSql(user.groupId)+")"
					}
					MaxResults(CH.config.maxGormRecords)
					order("firstname")
				}
			}
			
			result.parseQuery(users, maxResults)
			return result
		}
	}
	
	@Transactional(readOnly = false)
	private def withUser(User user, String id, Closure c) {
		if( !id ) throw new MissingPropertyException( NO_ID_SPECIFIED )
		
		final def targetUser = User.get(id)
		
		if(!targetUser) throw new InvalidPropertyException( NOT_FOUND + " [${id}]" )
		
		if( !securityService.canAdministerUser(user, targetUser) ) {
			log.warn 'User['+user.username+'] prevented from accessing user ['+targetUser.toString()+'].'
			throw new SecurityViolationException( NO_PERMISSION )
		}
		
		c.call targetUser
	}
}
