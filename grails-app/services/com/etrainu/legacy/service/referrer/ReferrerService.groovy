package com.etrainu.legacy.service.referrer

import groovy.lang.Closure;

import org.apache.commons.logging.LogFactory
import org.springframework.jmx.export.annotation.ManagedResource
import com.etrainu.legacy.model.referrer.Referrer
import com.etrainu.legacy.model.referrer.ReferrerDetails
import com.etrainu.legacy.model.training.Course
import org.springframework.transaction.annotation.Transactional
import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH

@ManagedResource(objectName = "etrainu:name=ReferrerService,type=service")
class ReferrerService {

    static transactional = true
	javax.sql.DataSource dataSource
	
	private static final log = LogFactory.getLog(this)
	def messageSource
	
	@Transactional(readOnly = false)
	def boolean saveReferrer(Referrer referrer) {
		def boolean status = false
		
		if(referrer.save(failOnError:true)) {
			log.info("saveReferrer succeeded for referrer: " + referrer.toString())						
			status = true
		}
		else {
			log.error("saveReferrer failed for referrer: " + referrer.toString())
			referrer.errors.allErrors.each { log.error messageSource.getMessage(it, null) }			
		}
		
		return status
	}
	
	@Transactional(readOnly = true)
	def getReferrer(String id) {	
		final def Referrer referrer = Referrer.get(id)
		return referrer
	}
	
	@Transactional(readOnly = false)
	def archiveReferrer(Referrer referrer) {
		referrer.archiveDate = new Date()
		log.info("archiveReferrer for referrer: " + referrer.toString())
		return saveReferrer(referrer)
	}
	
	@Transactional(readOnly = false)
	def unarchiveReferrer(Referrer referrer) {	
		referrer.archiveDate = null
		log.info("unarchiveReferrer for referrer: " + referrer.toString())
		return saveReferrer(referrer)
	}

	@Transactional(readOnly = true)
	def searchReferrers( String criteria, Boolean includeArchived )
	{
		def referrers = ReferrerDetails.findAllByNameIlikeOrIdIlike("%" + criteria + "%", "%" + criteria + "%")
		
		if( !includeArchived ) {
			referrers = referrers.findAll { it.archiveDate == null }
		}
		return referrers.sort{it.id.toLowerCase()}
	}

}
