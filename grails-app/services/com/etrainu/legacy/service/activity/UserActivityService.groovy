package com.etrainu.legacy.service.activity

import org.springframework.transaction.annotation.Transactional

import com.etrainu.legacy.activity.UserActivity
import com.etrainu.legacy.activity.UserActivity.UserActivityType
import com.etrainu.legacy.model.referrer.Transaction;
import com.etrainu.legacy.model.user.User
import com.etrainu.legacy.model.user.UserAssessment
import com.etrainu.legacy.model.user.UserCourse
import com.etrainu.legacy.model.user.UserLogging
import com.etrainu.legacy.model.user.UserScormAssessment

class UserActivityService {

    static transactional = true

	@Transactional(readOnly = true)
	def assessorActivity(User user, Date fromDate, Date toDate)
	{
		// a user as to be specified
		if (!user)
			return
		
		def final assessorAssessments
		if (fromDate && toDate)
		{
			assessorAssessments = UserAssessment.findAllByFk_assessorUserIDAndDtCompletedBetween(user.id, fromDate, toDate, [sort:"dtCompleted",order:"asc"])
		}
		else
		{
			assessorAssessments = UserAssessment.findAllByFk_assessorUserID(user.id, [sort:"dtCompleted",order:"asc"])
		}
		
		final proficientList = assessorAssessments.findAll {  it.isProficient == true }
		
		//Building list of activities grouping and counting by dates
		def activitySummariseByDate = countActivitiesSummariesByDate(proficientList,UserActivityType.MARKING_COMPETENT,"dtCompleted")
		return extractActivities(activitySummariseByDate)
	}
	
	@Transactional(readOnly = true)
	def assessmentActivity(User user, Date fromDate, Date toDate) 
	{
		// a user as to be specified
		if (!user)
			return
		
		def final userAssessments
		if (fromDate && toDate)
		{
			userAssessments = UserAssessment.findAllByUserIdAndDtStartedBetween(user.id, fromDate, toDate, [sort:"dtStarted",order:"asc"])
		}
		else
		{
			userAssessments = UserAssessment.findAllByUserId(user.id, [sort:"dtStarted",order:"asc"])
		}

		//Building list of activities grouping and counting by dates
		def activitySummariseByDate = countActivitiesSummariesByDate(userAssessments,UserActivityType.ASSESSMENT,"dtStarted")
		return extractActivities(activitySummariseByDate)
    }
	
	@Transactional(readOnly = true)
	def scormAssessmentActivity(User user, Date fromDate, Date toDate)
	{
		// a user as to be specified
		if (!user)
			return
		
		def final userAssessments
		if (fromDate && toDate)
		{
			userAssessments = UserScormAssessment.findAllByUserIdAndEntryDateBetween(user.id, fromDate, toDate, [sort:"entryDate",order:"asc"])
		}
		else
		{
			userAssessments = UserScormAssessment.findAllByUserId(user.id, [sort:"entryDate",order:"asc"])
		}

		//Building list of activities grouping and counting by dates
		def activitySummariseByDate = countActivitiesSummariesByDate(userAssessments,UserActivityType.SCORM_ASSESSMENT,"entryDate")
		return extractActivities(activitySummariseByDate)
	}
	
	@Transactional(readOnly = true)
	def courseAssignmentActivity(User user, Date fromDate, Date toDate)
	{
		// a user as to be specified
		if (!user)
			return
		
		def final userCourseAssignments
		if (fromDate && toDate)
		{
			userCourseAssignments = UserCourse.findAllByUserIdAndCreateDateBetween(user.id, fromDate, toDate, [sort:"createDate",order:"asc"])
		}
		else
		{
			userCourseAssignments = UserCourse.findAllByUserId(user.id, [sort:"createDate",order:"asc"])
		}

		//Building list of activities grouping and counting by dates
		def activitySummariseByDate = countActivitiesSummariesByDate(userCourseAssignments,UserActivityType.COURSE_ASSIGNMENT,"createDate")
		return extractActivities(activitySummariseByDate)

	}
	
	@Transactional(readOnly = true)
	def loginActivity(User user, Date fromDate, Date toDate)
	{
		// a user as to be specified
		if (!user)
			return
		
		def final userLogins
		if (fromDate && toDate)
		{
			userLogins = UserLogging.findAllByUserIdAndLogDateBetween(user.id, fromDate, toDate, [sort:"logDate",order:"asc"])
		}
		else
		{
			userLogins = UserLogging.findAllByUserId(user.id,[sort:"logDate",order:"asc"])
		}
		
		//Building list of activities grouping and counting by dates
		def activitySummariseByDate = countActivitiesSummariesByDate(userLogins,UserActivityType.LOGIN,"logDate")
		return extractActivities(activitySummariseByDate)
	}

	@Transactional(readOnly = true)
	def purchaseActivity(User user, Date fromDate, Date toDate)
	{
		// a user as to be specified
		if (!user)
			return
		
		def final purchases
		if (fromDate && toDate)
		{
			purchases = Transaction.findAllByPurchasedByUserIDAndDebitDateBetween(user.id, fromDate, toDate, [sort:"debitDate",order:"asc"])
		}
		else
		{
			purchases = Transaction.findAllByPurchasedByUserID(user.id,[sort:"debitDate",order:"asc"])
		}
		
		//Building list of activities grouping and counting by dates
		def activitySummariseByDate = countActivitiesSummariesByDate(purchases,UserActivityType.PURCHASE,"debitDate")
		return extractActivities(activitySummariseByDate)
	}
	
	private Map countActivitiesSummariesByDate(Collection collection, UserActivityType type, String dateColumn) {
		final activitiesSummariseByDate = [:]
		collection.each ()
		{
			if (it."$dateColumn")
			{
				def final String date = it."$dateColumn".format("yyyy-MM-dd")
				final UserActivity a = activitiesSummariseByDate.get(date)
				if (!a)
				{
					activitiesSummariseByDate.put(date, new UserActivity(userActivityType:type, userActivityDate:date, userActivityCount:1))
				}
				else
				{
					a.userActivityCount++
				}
			}
		}
		return activitiesSummariseByDate
	}
	
	private extractActivities(Map activitySummariseByDate) {
		final activities = []
		activitySummariseByDate.each() { activities.add(it.value)}
		return activities
	}
}
