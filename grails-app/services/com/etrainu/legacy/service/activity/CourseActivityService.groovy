package com.etrainu.legacy.service.activity

import org.springframework.transaction.annotation.Transactional

import com.etrainu.legacy.activity.CourseActivity
import com.etrainu.legacy.activity.CourseActivity.CourseActivityType
import com.etrainu.legacy.model.training.Course
import com.etrainu.legacy.model.user.UserCourse

class CourseActivityService {

	static transactional = true

	@Transactional(readOnly = true)
	def courseCompletionActivity(Course course, Date fromDate, Date toDate) {
		// a user as to be specified
		if (!course)
			return

		def final courseCompletions
		if (fromDate && toDate)
		{
			courseCompletions = UserCourse.findAllByCourseIdAndInductionCompetentDateBetween(course.id, fromDate, toDate, [sort:"inductionCompetentDate",order:"asc"])
		}
		else
		{
			courseCompletions = UserCourse.findAllByCourseId(course.id, [sort:"inductionCompetentDate",order:"asc"])
		}

		//Building list of activities grouping and counting by dates
		def activitySummariseByDate = countActivitiesSummariesByDate(courseCompletions,CourseActivityType.COURSE_COMPLETION,"inductionCompetentDate")
		return extractActivities(activitySummariseByDate)
	}

	@Transactional(readOnly = true)
	def courseAssignmentActivity(Course course, Date fromDate, Date toDate) {
		// a user as to be specified
		if (!course)
			return

		def final courseAllocations
		if (fromDate && toDate)
		{
			courseAllocations = UserCourse.findAllByCourseIdAndAccessAllowedDateBetween(course.id, fromDate, toDate, [sort:"accessAllowedDate",order:"asc"])
		}
		else
		{
			courseAllocations = UserCourse.findAllByCourseId(course.id, [sort:"accessAllowedDate",order:"asc"])
		}

		//Building list of activities grouping and counting by dates
		def activitySummariseByDate = countActivitiesSummariesByDate(courseAllocations,CourseActivityType.COURSE_ASSIGNMENT,"accessAllowedDate")
		return extractActivities(activitySummariseByDate)
	}
	
	private Map countActivitiesSummariesByDate(Collection collection, CourseActivityType type, String dateColumn) {
		final activitiesSummariseByDate = [:]
		collection.each ()
		{
			if (it."$dateColumn")
			{
				def final String date = it."$dateColumn".format("yyyy-MM-dd")
				final CourseActivity a = activitiesSummariseByDate.get(date)
				if (!a)
				{
					activitiesSummariseByDate.put(date, new CourseActivity(courseActivityType:type, courseActivityDate:date, courseActivityCount:1))
				}
				else
				{
					a.courseActivityCount++
				}
			}
		}
		return activitiesSummariseByDate
	}

	private extractActivities(Map activitySummariseByDate) {
		final activities = []
		activitySummariseByDate.each() { activities.add(it.value)}
		return activities
	}

}






