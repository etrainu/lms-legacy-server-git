package com.etrainu.legacy.service.activity

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.etrainu.legacy.activity.ReferrerActivity
import com.etrainu.legacy.activity.ReferrerActivity.ReferrerActivityType
import com.etrainu.legacy.activity.SalesActivity;

import org.springframework.transaction.annotation.Transactional
import com.etrainu.legacy.model.referrer.Referrer
import com.etrainu.legacy.model.referrer.Transaction
import com.etrainu.legacy.model.training.Course;

class SalesActivityService {

	static transactional = true
	
	@Transactional(readOnly = true)
	def salesActivity(Date fromDate, Date toDate) {
		
		def final sales
		if (fromDate && toDate)
		{	sales = Transaction.findAllByDebitDateBetween(fromDate, toDate, [sort:"debitDate",order:"asc"]) }
		else
		{	sales = Transaction.list() 	}
		
		//Group the results by Induction Id : result in a map keyed by inductionId
		final def salesGroupedByProduct = sales.groupBy({it.inductionId})
		
		//For each entry in the resulting , process the values into SaleActivity
		final def resultMap = [:]
		salesGroupedByProduct.each { 
			resultMap.put(it.key,extractActivities(countActivitiesSummariesByDate(it.value,it.key, "debitDate"))) 
			}
		
		return resultMap
	}
	
	private Map countActivitiesSummariesByDate(Collection collection,String inductionId, String dateColumn) {
		final activitiesSummariseByDate = [:]
		collection.each ()
		{
			if (it."$dateColumn")
			{
				def final String date = it."$dateColumn".format("yyyy-MM-dd")
		
				final SalesActivity a = activitiesSummariseByDate.get(date)
				if (!a)
				{
					activitiesSummariseByDate.put(date, new SalesActivity( salesActivityDate:date, salesActivityCount:1, salesActivityIncome:(it.amount ? it.amount : 0), inductionId: inductionId))
				}
				else
				{
					a.salesActivityCount++
					a.salesActivityIncome+= (it.amount ? it.amount : 0)
				}
			}
		}
		return activitiesSummariseByDate
	}

	private extractActivities(Map activitySummariseByDate) {
		final activities = []
		activitySummariseByDate.each() { activities.add(it.value)}
		return activities
	}
}
