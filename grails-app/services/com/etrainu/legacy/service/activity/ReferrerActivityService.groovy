package com.etrainu.legacy.service.activity

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.etrainu.legacy.activity.ReferrerActivity
import com.etrainu.legacy.activity.ReferrerActivity.ReferrerActivityType
import org.springframework.transaction.annotation.Transactional
import com.etrainu.legacy.model.referrer.Referrer
import com.etrainu.legacy.model.referrer.Transaction
import com.etrainu.legacy.model.training.Course;

class ReferrerActivityService {

	static transactional = true
	
	@Transactional(readOnly = true)
	def salesActivity(Referrer referrer, Date fromDate, Date toDate) {
		// a user as to be specified
		if (!referrer)
			return

		def final sales
		if (fromDate && toDate)
		{
			if (referrer.isDiscount)
				sales = Transaction.findAllByPromoCodeAndDebitDateBetween(referrer.id, fromDate, toDate, [sort:"debitDate",order:"asc"])
			else
				sales = Transaction.findAllByReferrerDomainAndDebitDateBetween(referrer.id, fromDate, toDate, [sort:"debitDate",order:"asc"])
		}
		else
		{
			if (referrer.isDiscount)
				sales = Transaction.findAllByPromoCode(referrer.id, [sort:"debitDate",order:"asc"])
			else
				sales = Transaction.findAllByReferrerDomain(referrer.id, [sort:"debitDate",order:"asc"])
		}

		//Building list of activities grouping and counting by dates
		def activitySummariseByDate = countActivitiesSummariesByDate(sales,ReferrerActivityType.SALES,"debitDate")
		return extractActivities(activitySummariseByDate)
	}
	
	private Map countActivitiesSummariesByDate(Collection collection, ReferrerActivityType type, String dateColumn) {
		final activitiesSummariseByDate = [:]
		collection.each ()
		{
			if (it."$dateColumn")
			{
				def final String date = it."$dateColumn".format("yyyy-MM-dd")
				def final Double amount = 0
				if (it.amount) {
					amount = it.amount
				}
				final ReferrerActivity a = activitiesSummariseByDate.get(date)
				if (!a)
				{
					activitiesSummariseByDate.put(date, new ReferrerActivity(referrerActivityType:type, referrerActivityDate:date, referrerActivityCount:1, referrerActivityIncome:amount))
				}
				else
				{
					a.referrerActivityCount++
					a.referrerActivityIncome+=amount
				}
			}
		}
		return activitiesSummariseByDate
	}

	private extractActivities(Map activitySummariseByDate) {
		final activities = []
		activitySummariseByDate.each() { activities.add(it.value)}
		return activities
	}
}
