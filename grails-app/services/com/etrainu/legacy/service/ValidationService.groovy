package com.etrainu.legacy.service

/*
 * The purpose of the validation service is to return a map to determine the success of any given validation
 * If the validation is a failure, the service should return a valid map for directly being rendered to simplify
 * Example: 
 * 		def validationResult = validationService.validateUser( request )
 * 		if( !validationResult.success ) { render( validationResult ) }
 */

import java.util.Map
import org.codehaus.groovy.grails.commons.ConfigurationHolder as CH
import com.etrainu.legacy.model.user.User

class ValidationService {
	
	def renderService
	def userService
	
	static transactional = false
	
	private final static String APIUsername = 'etrainuAPI'
	private final static String APIPassword = 'eRt]UN_7!$1'
	
	def validateAPIUser(Object request)
	{
		
		String key = request.getHeader('key');
		
		if( !key ) {
			return renderService.renderResult("Request must include an authentication key (key=) in the header to validate API access.", renderService.Unauthorized)
		}
		
		User usr = User.findByAuthKey(key)
		
		if( !usr ) {
			return renderService.renderResult("Invalid or unrecognized API Key found.", renderService.Unauthorized)
		}
		
		//Start with right now
		def authKeyCutOff = Calendar.getInstance()
		//Calculate what the earliest time would be
		authKeyCutOff.add CH.config.authKeyTimeoutUnit, -(CH.config.authKeyTimeoutValue)
		
		def userLastAuthActivityDate = Calendar.getInstance()
		userLastAuthActivityDate.setTime( usr.authKeyLastUsed )
		
		if( !authKeyCutOff.before(userLastAuthActivityDate) ) {
			return renderService.renderResult("API Key has expired.", renderService.Unauthorized)
		}
		
		usr.authKeyLastUsed = Calendar.getInstance().getTime()
		userService.saveUser( usr )

		return [success:true]
	}
	
	def validateEtrainuAPIUser(Object request) {
		String usr = request.getHeader('u')
		String pas = request.getHeader('p')

		if( !usr || !pas ) {
			return renderService.renderResult("Request must include a username (u=) and password (p=) in the header to validate API access", renderService.Unauthorized)
		}

		if( !usr.equals(APIUsername) || !pas.equals(APIPassword) ) {
			return renderService.renderResult("Invalid Username or Password in API validation", renderService.Unauthorized)
		}

		return [success:true]
	}

	def validatePostRequest(Map params) {
		if( !params.xml ) {
			return renderService.renderResult("Request must include a xml property.", renderService.BadRequest)
		}

		return [success:true]
	}
}
