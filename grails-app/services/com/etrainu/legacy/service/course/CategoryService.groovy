package com.etrainu.legacy.service.course

import org.springframework.transaction.annotation.Transactional

import com.etrainu.legacy.model.training.Category

class CategoryService {

    static transactional = true
	
	@Transactional(readOnly = true)
    def getAllPublic() 
	{
		def categories = Category.findAllByIsPublic(true)
		return categories
    }
	
	
}
