package com.etrainu.legacy.service.course

import groovy.lang.Category
import groovy.sql.Sql
import groovy.util.ConfigObject

import org.apache.commons.logging.LogFactory
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.springframework.jmx.export.annotation.ManagedResource
import org.springframework.transaction.annotation.Transactional

import com.etrainu.legacy.data.CourseKey
import com.etrainu.legacy.model.training.Course
import com.etrainu.legacy.model.training.CourseDetails
import com.etrainu.legacy.model.training.Key
import com.etrainu.legacy.model.user.User
import com.etrainu.search.SearchResult

@ManagedResource(objectName = "etrainu:name=CourseService,type=service")
class CourseService {
	
	private static final log = LogFactory.getLog(this)
	final private ConfigObject config = ConfigurationHolder.config
	
    static transactional = true
	javax.sql.DataSource dataSource
	
	@Transactional(readOnly = false)
	def saveCourse(Course course) {
		if(course.save(failOnError:true)) {
			return course
		}
		else {
			course.errors.allErrors.each { println it }
		}
	}
	
	@Transactional(readOnly = true)
	def getCourse(String courseId) {
		Course.get(courseId)
	}
	
	@Transactional(readOnly = false)
	def archiveCourse(Course course) {
		course.archiveDate = new Date()
		return saveCourse( course )
	}
	
	@Transactional(readOnly = false)
	def unarchiveCourse(Course course) {
		course.archiveDate = null
		return saveCourse( course )
	}

	@Transactional(readOnly = true)
    def getCoursesForUser(User u) {

		final def userKeys = Key.findAllByUserId(u.id)
		def courseList = [];
		
		for (Key k : userKeys)
		{	def CourseKey ck = new CourseKey(course:k.course, key:k)
			courseList.add ck
		}
		
		return courseList.sort{it.course.name}	
    }
	
	@Transactional(readOnly = true)
	def searchCourses( String courseName, Boolean includeArchived )
	{
		if( includeArchived ) {
			return Course.findAllByNameLike("%" + courseName + "%")
		} else {
			return Course.findAllByNameLikeAndArchiveDateIsNull("%" + courseName + "%")
		}
		
	}
	
	@Transactional(readOnly = true)
	def getCoursesForCategory(Category category)
	{
		final List courses = []
		final sql = new Sql(dataSource)
		def rows = sql.rows("SELECT fk_inductionID FROM tbl_courseInCategory WHERE fk_categoryID = ${cat.id}")
		rows.each{ row ->
			def course = Course.findById(row.fk_inductionID)
			if( course ) courses.add course
		}
	}
	
	@Transactional(readOnly = true)
	def quickSearch(String searchString, Boolean includeArchived) {
		return quickSearch(searchString, includeArchived, config.maxGormRecords)
	}
	
	@Transactional(readOnly = true)
	def quickSearch(String searchString, Boolean includeArchived, Integer maxResults) {
		def timeStart = new Date().time
		def result = new SearchResult()
		
		def final courses = CourseDetails.findAllBySearchDataIlike('%' + searchString + '%', [sort:"inductionName"])
		
		if( !includeArchived ) {
			courses = courses.findAll { it.archiveDate == null }
		}
		
		result.parseQuery(courses, maxResults)
		
		def timeEnd = new Date().time
		log.info("QuickSearch Returned (" + result.results.size() + ") Results in (" + (timeEnd-timeStart) + ')ms from (' + result.maxResults + ') Records' )
		
		return result
	}
	
}
