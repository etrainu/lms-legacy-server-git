package com.etrainu.legacy.service.xml

import java.io.StringWriter

import com.etrainu.legacy.activity.CourseActivity
import com.etrainu.legacy.activity.ReferrerActivity
import com.etrainu.legacy.activity.UserActivity
import com.etrainu.legacy.model.referrer.Referrer
import com.etrainu.legacy.model.training.Course
import com.etrainu.legacy.model.user.User
import com.etrainu.legacy.model.user.UserDetails
import com.etrainu.legacy.service.course.CourseService
import com.etrainu.search.SearchResult
import com.etrainu.xml.XMLSerializable

class RenderService {

	static transactional = false
	
	public static final int Ok = 200 //Successful
	public static final int Accepted = 202 //Accepted but not acknowledged
	public final static int BadRequest = 400 //Malformed syntax
	public final static int Unauthorized = 401 //Not authorized (user/pass in header is incorrect)
	public final static int Unsupported = 415 //Used post when get is required etc
	
	private static String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
	
	CourseService courseService
	
	public renderXMLSerializable(XMLSerializable serializableObject) {
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		XMLResponse.append serializableObject.toXML()
		
		return XMLResponse
	}
	
	public renderCourses(ArrayList<Course> courses) {
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.courses{
			courses.each() { cours -> /*Course is already being used*/
				cours.toXMLNode(builder)
			}
		}

		return XMLResponse
	}
	
	public renderCourse(Course course)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		XMLResponse.append course.toXML()
		
		return XMLResponse
	}
	
	public renderUsers(ArrayList<User> users) {
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.users{
			users.each() { usr ->
				usr.toXMLNode(builder, true)
			}
		}

		return XMLResponse
	}
	
	public renderUserDetails(ArrayList<UserDetails> users) {
		return renderUserDetails(users, users.size())
	}
	
	public renderUserDetails(ArrayList<UserDetails> users, Integer totalResults) {
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.users(totalResults:totalResults){
			users.each() { usr ->
				usr.toXMLNode(builder)
			}
		}

		return XMLResponse
	}
	
	public renderSearchResult(SearchResult result) {
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.results(totalResults:result.maxResults){
			result.results.each() {
				it.toXMLNode(builder)
			}
		}

		return XMLResponse
	}
	
	public renderUser(User user)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		XMLResponse.append user.toXML()
		
		return XMLResponse
	}
	
	public renderCategories(categoryList)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.categories{
			for(c in categoryList) {
				category(
						id:c.id
						//,href: ConfigurationHolder.config.grails.serverURL + "/" + params.controller + '/search/' + c.id
						){
							name(c.name)
						}
			}
		}
		
		return XMLResponse
	}
	
	public renderProviders(providersMap)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.providers{
			for ( p in providersMap ) 
			{
				provider( id:p.key )
				{
					name(p.value)
				}
			}
		}
		
		return XMLResponse
	}
	
	public renderUserCourses(userCourses)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)

		builder.courses{
			for(c in userCourses) {
				course(
						id:c.courseId, key:c.id
						//,href: ConfigurationHolder.config.grails.serverURL + "/" + params.controller + '/search/' + c.id
						){
							name(c.inductionName)
							status(c.status)
							accessDate(c.accessAllowedDate)
						}
			}
		}
		
		return XMLResponse
	}
	
	public renderUserPartnerships(userPartnerships)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.partnerships{
			userPartnerships.each() { part ->
				part.toXMLNode(builder)
			}
		}

		return XMLResponse
	}
	
	public renderGroups(groups)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.groups{
			for(grp in groups) {
				group(id:grp.id) {
					id(grp.id)
					groupTypeId(grp.fk_groupTypeId)
					parentGroup(grp.parentGroup)
					groupCode(grp.groupCode)
					groupName(grp.groupName)
					expiryDate(grp.expiryDate)
					active(grp.active)
					archiveDate(grp.archiveDate)
					isRTO(grp.isRTO)
					suborgGroupingId(grp.fk_suborgGroupingId)
				}
				
			}
		}
		
		return XMLResponse	
	}
	
	public renderUserActivities(activities)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.userActivities{
			for(UserActivity a in activities) {
				userActivity(activityDate:a.userActivityDate, activityType:a.userActivityType.toString(), activityCount:a.userActivityCount)
			}
		}
		return XMLResponse
	}
	
	public renderCourseActivities(activities)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.courseActivities{
			for(CourseActivity a in activities) {
				courseActivity(activityDate:a.courseActivityDate, activityType:a.courseActivityType.toString(), activityCount:a.courseActivityCount)
			}
		}
		return XMLResponse
	}
	
	public renderReferrerActivities(activities)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.referrerActivities{
			for(ReferrerActivity a in activities) {
				referrerActivity(activityDate:a.referrerActivityDate, activityType:a.referrerActivityType.toString(), activityCount:a.referrerActivityCount, activityIncome:a.referrerActivityIncome)
			}
		}
		return XMLResponse
	}
	
	public renderSalesActivities(Map activitiesMapByProduct)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.salesActivities{
			activitiesMapByProduct.each{p ->
				final Course c = courseService.getCourse(p.key)
				if (c)
				{
					product(id:c.id, name:c.name) {
						activitiesMapByProduct[p.key].each{ sa ->
							saleActivity(salesActivityDate:sa.salesActivityDate, salesActivityCount:sa.salesActivityCount, salesActivityIncome:sa.salesActivityIncome)
						}
					}
				}
			}
		}
		return XMLResponse
	}
	
	public renderReferrer(Referrer referrer)
	{
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		XMLResponse.append referrer.toXML()
		
		return XMLResponse
	}
	
	public renderReferrers(ArrayList<Referrer> referrers) {
		final StringWriter XMLResponse = new StringWriter()
		final def builder = getXMLBuilder(XMLResponse)
		
		builder.referrers{
			referrers.each() { ref -> /*Referrer is already being used*/
				ref.toXMLNode(builder)
			}
		}

		return XMLResponse
	}
	
	public renderResult(String text)
	{
		return renderResult(text, this.Ok)
	}
	
	public renderResult(String text, int statusCode) {
		StringWriter XMLResponse = new StringWriter()
		def builder = getXMLBuilder(XMLResponse)

		builder.result {
			message(text)
		}
		
		def renderMap = [text: XMLResponse.toString()
			, contentType:"text/xml"
			, encoding:"UTF-8"
			, status:statusCode]
		return renderMap
	}
	
	static groovy.xml.MarkupBuilder getXMLBuilder(StringWriter XMLResponse) 
	{
		XMLResponse.append xmlHeader
		final def builder = new groovy.xml.MarkupBuilder(XMLResponse)
		return builder
	}
}
