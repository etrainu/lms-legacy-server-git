package com.etrainu.security

import org.springframework.transaction.annotation.Transactional

import com.etrainu.legacy.model.group.Group
import com.etrainu.legacy.model.user.Partnership
import com.etrainu.legacy.model.user.User

class SecurityService {

	static transactional = true
	
	def userService
	
	@Transactional(readOnly = true)
	public Boolean canAdministerGroup(User user, Integer groupId) {
		//If user has no group or users group is 0
		if( !user.groupId ) return false
		
		//User has direct link to target group or is an admin of the system
		if( user.groupId == groupId || isAdministrator(user) ) return true
		
		//Check if the user has cascading permission to administer
		def groups = Group.createCriteria().list{
			eq('id', groupId )
			sqlRestriction "groupid in (" + groupSegregationSql(user.groupId) + ")"
		}
		if( groups.size() ) {
			return true
		}
		
		//Default
		return false
	}
	
	@Transactional(readOnly = true)
	public Boolean canAdministerPartnership(User user, Partnership partnership) {
		/* Users can administer partnerships at the same level as them */
		/* Can AdministerUser will also account for the userid being the same */
		def canAdminUser = canAdministerUser(user, partnership.userId)
		def sameGroup = (user.groupId == partnership.groupId)
		def canAdminGroup = canAdministerGroup( user, partnership.groupId)
		
		if( (sameGroup || canAdminGroup) && canAdminUser ) return true
		
		return false
	}
	
	@Transactional(readOnly = true)
	public Boolean canAdministerUser(User user, String userId) {
		return canAdministerUser(user, userService.getUser(userId))
	}
	
	@Transactional(readOnly = true)
	public Boolean canAdministerUser(User user, User targetUser) {
		//Admins can admin all and a user can always administer themself
		if( isAdministrator(user) || user.id.equals(targetUser.id) ) return true
		
		//Users can't administer users of the same level
		if( user.groupId == targetUser.groupId || !user.groupId ) return false
		
		//Determine if the user that is being administered is a admin or participant
		//If the targetuser groupid is null or 0
		if( !targetUser.groupId ) {
			/*
			 * TODO: Currently, this will test against the participants partnerships or TRUE if they are B2C
			 * This is not ideal. Needs to be revised.
			 */
			//targetGroup = 
			def groups = Group.createCriteria().list {
				sqlRestriction """\
					groupId in (
						SELECT fk_groupId 
						FROM tbl_participantGroup 
						WHERE fk_userID = '${targetUser.id}'
						AND acceptedByUser = 1
						AND acceptedByGroup = 1
						AND dateSevered is NULL
					)
				"""
			}
			
			if( !groups.size() )
				return true //Free floating/b2c Participant
				
			if( groups.size() ) {
				def canAdmin = false
				//account for multiple possibles here
				groups.each { group ->
					if( canAdministerGroup(user, group.id) ) {
						canAdmin = true
					}
				}
				return canAdmin
			}
		} else {
			return canAdministerGroup(user, targetUser.groupId)
		}
	}
	
	//This could be handy elsewhere
	@Transactional(readOnly = true)
	public Boolean isAdministrator(User user) {
		def groups = Group.createCriteria().list{
			eq('id', user.groupId )
			sqlRestriction "groupid in (SELECT groupId FROM tbl_userGroup INNER JOIN tbl_groupType ON groupTypeID = fk_groupTypeID AND type = 'ADMIN')"
		}
		if( groups.size() ) return true;
	}
	
	// SQL for a list of groups that a groupId should have access to
	@Transactional(readOnly = true)
	public String groupSegregationSql(Integer groupId) {
		/*
		 * Since the highest level group other than admin is orgAdmin, we only need to go 3 levels lower to list all groups
		 * that this person can administer
		 */
		"""\
			SELECT groupID
			FROM tbl_userGroup
			WHERE parentGroup = ${groupId}
			
			UNION
			
			SELECT groupID
			FROM tbl_userGroup
			WHERE parentGroup in (
				SELECT groupID
				FROM tbl_userGroup
				WHERE parentGroup = ${groupId}
			)
			
			UNION
			
			SELECT groupID
			FROM tbl_userGroup
			WHERE parentGroup in (
				SELECT groupID
				FROM tbl_userGroup
				WHERE parentGroup in (
					SELECT groupID
					FROM tbl_userGroup
					WHERE parentGroup = ${groupId}
				)
			)
		"""
	}
}
