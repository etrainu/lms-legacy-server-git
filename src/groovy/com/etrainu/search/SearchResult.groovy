package com.etrainu.search

import com.etrainu.xml.XMLSerializable

class SearchResult {
	
	public Integer maxResults
	public List<XMLSerializable> results
	
	SearchResult(List<XMLSerializable> results, Integer maxResults) {
		this.results = results
		this.maxResults = maxResults
	}
	
	SearchResult(List<XMLSerializable> results) {
		this(results, results.size())
	}
	
	SearchResult() {
		this([])
	}
	
	//This function will take a gorm query result and truncate it to the required size, setting the appropriate properties
	public parseQuery(List<XMLSerializable> query, Integer maximumNumberOfResultsToReturn) {
		this.maxResults = query.size()

		def lastIndex = ( maximumNumberOfResultsToReturn < query.size() ) ? maximumNumberOfResultsToReturn : query.size();
		// TODO: Test and replace this with query.subList as pagedResult does not include removeRange
		if( lastIndex < query.size() ) query.removeRange( lastIndex, query.size() )
		
		this.results = query
	}
	
	public getAt(Integer index) {
		return this.results[index]
	}
}
