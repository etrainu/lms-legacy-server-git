package com.etrainu.exception.security

import org.codehaus.groovy.grails.exceptions.GrailsException

class SecurityViolationException extends GrailsException {
	def SecurityViolationException(String message) {
		super(message)
	}
	
	def SecurityViolationException(String message, Throwable cause) {
		super(message, cause)
	}
	
	def SecurityViolationException(Throwable cause) {
		super(cause)
	}
}