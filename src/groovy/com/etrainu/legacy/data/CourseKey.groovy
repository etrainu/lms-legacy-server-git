package com.etrainu.legacy.data

import com.etrainu.legacy.model.training.Course
import com.etrainu.legacy.model.training.Key


class CourseKey {
	Key key
	Course course
}
