package com.etrainu.legacy.utils

import org.codehaus.groovy.grails.commons.*

import com.etrainu.legacy.validation.DateValidationResponse

static class DateUtils {
	def final static config = ConfigurationHolder.config

	public static DateValidationResponse validateFromAndToDate(String fromDateAsString, String toDateAsString) {
		final DateValidationResponse res = new DateValidationResponse(hasErrors:false)

		final Date fromDate
		if (fromDateAsString) {
			try {
				fromDate = Date.parse(config.etrainu.api.date.DATEFORMAT, fromDateAsString)
			}
			catch(e) {
				res.hasErrors = true
				res.errorMessage = "Could not parse [from] param to a date  : " + fromDateAsString
				return res
			}
			res.fromDate = fromDate
		}

		final Date toDate
		if (toDateAsString) {
			try {
				toDate = Date.parse(config.etrainu.api.date.DATEFORMAT, toDateAsString)
			}
			catch(e) {
				res.hasErrors = true
				res.errorMessage = "Could not parse [to] param to a date : " + toDateAsString
				return res
			}

			res.toDate = toDate
		}

		if (res.fromDate && res.toDate) {
			if (res.fromDate>=res.toDate) {
				res.hasErrors = true
				res.errorMessage = "[to] date is before [from] date"
				return res
			}

			println "ckecking for max range " + config.etrainu.api.date.MAXRANGE
			println "between day " + daysBetween(res.fromDate,res.toDate)
			
			if (daysBetween(res.fromDate,res.toDate) > config.etrainu.api.date.MAXRANGE.toInteger()) {
				res.hasErrors = true
				res.errorMessage = "Your date range is too large."
				return res
			}
		}

		return res
	}

	static def daysBetween(def startDate, def endDate) {
		use(groovy.time.TimeCategory) {
			def duration = endDate - startDate
			duration.days
		}
	}
}
