package com.etrainu.legacy.utils

import groovy.lang.Closure

import org.apache.commons.logging.LogFactory
import org.apache.log4j.Logger;

class MonitoringUtils {
	
	private static final log = LogFactory.getLog(this)

	public static def benchmark(String methodName, Closure closure) {
		
		final start = System.currentTimeMillis()
		final def result = closure.call()
		final now = System.currentTimeMillis()
		log.info "${methodName} execution took ${now - start} ms"
		return result
	  }
}
