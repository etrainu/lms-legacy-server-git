package com.etrainu.legacy.utils

import com.etrainu.legacy.model.user.User

static class UserUtils 
{
	private static User findUserbyUUIDorUsername(String userid)
	{
		//UUID is always 36 characters long and contains a digit. This is most likely a UUID
		if (userid.length()==36 && userid.find(/\d+/))
		{
			return User.findById(userid)
		}
		else //Try by username
		{
			return User.findByUsername(userid)
		}
	}
}
