package com.etrainu.legacy.activity

class UserActivity {

	def public static final enum UserActivityType {
		SCORM_ASSESSMENT,
		ASSESSMENT, 
		LOGIN,
		COURSE_ASSIGNMENT,
		MARKING_COMPETENT,
		PURCHASE
	}
	
	String userActivityDate
	Integer userActivityCount
	UserActivityType userActivityType	
}
