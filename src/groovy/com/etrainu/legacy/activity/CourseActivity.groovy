package com.etrainu.legacy.activity

class CourseActivity {

	def public static final enum CourseActivityType {
		COURSE_ASSIGNMENT,
		COURSE_COMPLETION
	}
	
	String courseActivityDate
	Integer courseActivityCount
	CourseActivityType courseActivityType	
}
