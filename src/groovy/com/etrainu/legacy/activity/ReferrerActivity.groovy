package com.etrainu.legacy.activity

class ReferrerActivity {
	
	def public static final enum ReferrerActivityType {
		SALES
	}
	
	String referrerActivityDate
	Integer referrerActivityCount
	Double referrerActivityIncome
	ReferrerActivityType referrerActivityType

}
