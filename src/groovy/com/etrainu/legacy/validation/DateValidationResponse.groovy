package com.etrainu.legacy.validation

class DateValidationResponse 
{
	String errorMessage
	boolean hasErrors
	Date fromDate
	Date toDate
}
